//
//  SplashScreenContract.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SplashViewController;
@class RadioStation;

@protocol SplashScreenPresenter;
@protocol SplashScreenView;
@protocol SplashScreenInteractorInPut;
@protocol SplashScreenInteractorOutPut;

@protocol SplashScreenWireframe <NSObject>

@property (weak, nonatomic) SplashViewController* presentedSplashScreen;

+ (UIViewController*)assembleModule;
- (void)presendRadioStationsScreenWithStations:(NSArray<RadioStation*>*)radioStations;

@end

@protocol SplashScreenView <NSObject>

@property (strong, nonatomic) id<SplashScreenPresenter>presenter;

- (void)showPopupWithError:(NSError*)error;
- (void)showActivityIndicaror;
- (void)hideActivityIndicator;

@end

@protocol SplashScreenPresenter <NSObject>

@property (strong, nonatomic) id<SplashScreenInteractorInPut>interactorIn;
@property (weak, nonatomic)   id<SplashScreenView>view;
@property (strong, nonatomic) id<SplashScreenWireframe>router;

- (void)splashScreenLoaded;
- (void)splashScreenDismissed;
- (void)repeatOccuredOnSplashScreen;

@end

@protocol SplashScreenInteractorInPut <NSObject>

@property (weak, nonatomic) id<SplashScreenInteractorOutPut>interactorOut;

- (void)startLoadingRadioStations;

@end

@protocol SplashScreenInteractorOutPut <NSObject>

- (void)radioStationsFetchedCompletely:(NSArray<RadioStation*>*)radioStations;
- (void)radioStationsNotFetchedWithError:(NSError*)error;

@end

//
//  SplashViewController.h
//  RadioRecordApp
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "SplashScreenContract.h"

@interface SplashViewController : UIViewController <SplashScreenView>

@property (strong, nonatomic) id<SplashScreenPresenter>presenter;

+ (SplashViewController*)instanceFromStoryboard;

@end

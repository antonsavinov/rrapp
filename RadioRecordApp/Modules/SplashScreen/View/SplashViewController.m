//
//  SplashViewController.m
//  RadioRecordApp
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "SplashScreenPresenter.h"

#import "SplashViewController.h"

#import "AlertView.h"

@interface SplashViewController () <AlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation SplashViewController

#pragma mark - Life cycle

+ (SplashViewController *)instanceFromStoryboard
{
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SplashViewController* splashScreenViewController =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"splashScreenControllerIdentifier"];
    
    return splashScreenViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.presenter splashScreenLoaded];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.presenter splashScreenDismissed];
}

- (void)dealloc
{
    RRLog(@"SplashViewController dismissed");
}

#pragma mark - Private

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - SplashScreenView

- (void)showActivityIndicaror
{
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator
{
    [self.activityIndicator stopAnimating];
}

- (void)showPopupWithError:(NSError *)error {
    
    AlertView* alertView = [[AlertView alloc] initWithTitle:Localized(@"IDS_ALERT_VIEW_ERROR_TITLE")
                                                    message:error.description
                                                   delegate:self
                                                    buttons:@[Localized(@"IDS_ALERT_VIEW_BUTTON_OK"),
                                                              Localized(@"IDS_ALERT_VIEW_BUTTON_REPEAT")]];
    [alertView showAboveView:self.view];
}

#pragma mark - AlertViewDelegate

- (void)alertView:(AlertView *)alertView wasTappedByButtonWithIndex:(NSUInteger)index {
    if (index == 1) {
        [self.presenter repeatOccuredOnSplashScreen];
    }
}

@end

//
//  SplashScreenRouter.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenContract.h"

@interface SplashScreenRouter : NSObject <SplashScreenWireframe>

@property (weak, nonatomic) SplashViewController* presentedSplashScreen;

@end

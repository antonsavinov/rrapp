//
//  SplashScreenRouter.m
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenInteractor.h"
#import "SplashScreenPresenter.h"
#import "SplashScreenRouter.h"
#import "RadioStationsRouter.h"

#import "SplashViewController.h"

#import "RadioStationsViewController.h"

@implementation SplashScreenRouter

#pragma mark - SplashScreenWireframe

+ (UIViewController*)assembleModule {
    
    SplashViewController* splashViewController = [SplashViewController instanceFromStoryboard];
    
    SplashScreenPresenter* splashScreenPresenter = [SplashScreenPresenter new];
    SplashScreenInteractor* splashScreenInteractor = [SplashScreenInteractor new];
    SplashScreenRouter* splashScreenRouter = [SplashScreenRouter new];

    splashViewController.presenter = splashScreenPresenter;

    splashScreenPresenter.interactorIn = splashScreenInteractor;
    splashScreenPresenter.view = splashViewController;
    splashScreenPresenter.router = splashScreenRouter;

    splashScreenInteractor.interactorOut = splashScreenPresenter;
    
    splashScreenRouter.presentedSplashScreen = splashViewController;

    return splashViewController;
}

- (void)presendRadioStationsScreenWithStations:(NSArray<RadioStation *> *)radioStations {
    RadioStationsViewController* radioStationsScreen =
    [RadioStationsRouter assembleModuleForRadioStations:radioStations];
    radioStationsScreen.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    UINavigationController* navigationStack =
    [[UINavigationController alloc] initWithRootViewController:radioStationsScreen];
    navigationStack.navigationBarHidden = YES;
    
    [self.presentedSplashScreen presentViewController:navigationStack animated:YES completion:nil];
}

@end

//
//  SplashScreenInteractor.m
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenInteractor.h"
#import "RRManager.h"

@implementation SplashScreenInteractor

#pragma mark - Life cycle

- (void)dealloc
{
    RRLog(@"SplashScreenInteractor dismissed");
}

#pragma mark - SplashScreenInteractorInPut

- (void)startLoadingRadioStations
{
    [[RRManager sharedInstance] fetchListOfStationsWithCompletionBlock:^(NSArray<RadioStation *> *radioStatios) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.interactorOut radioStationsFetchedCompletely:radioStatios];
        });

    } andErrorBlock:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.interactorOut radioStationsNotFetchedWithError:error];
        });
        
    }];
}



@end

//
//  SplashScreenInteractor.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenContract.h"

@interface SplashScreenInteractor : NSObject <SplashScreenInteractorInPut>

@property (weak, nonatomic) id<SplashScreenInteractorOutPut>interactorOut;

@end

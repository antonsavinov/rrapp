//
//  SplashScreenPresenter.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenContract.h"

@interface SplashScreenPresenter : NSObject <SplashScreenPresenter>

@property (strong, nonatomic) id<SplashScreenInteractorInPut>interactorIn;
@property (weak, nonatomic)   id<SplashScreenView>view;
@property (strong, nonatomic) id<SplashScreenWireframe>router;

@end

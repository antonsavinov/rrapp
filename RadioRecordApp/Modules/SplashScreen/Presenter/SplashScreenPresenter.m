//
//  SplashScreenPresenter.m
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "SplashScreenPresenter.h"

@interface SplashScreenPresenter () <SplashScreenInteractorOutPut>

@end

@implementation SplashScreenPresenter

#pragma mark - Life cycle

- (void)dealloc
{
    RRLog(@"SplashScreenPresenter dismissed");
}

#pragma mark - SplashScreenPresenter

- (void)splashScreenLoaded
{
    [self.view showActivityIndicaror];
    [self.interactorIn startLoadingRadioStations];
}

- (void)splashScreenDismissed
{
    
}

- (void)radioStationsFetchedCompletely:(NSArray<RadioStation *> *)radioStations
{
    [self.router presendRadioStationsScreenWithStations:radioStations];
}

- (void)radioStationsNotFetchedWithError:(NSError *)error
{
    [self.view hideActivityIndicator];
    [self.view showPopupWithError:error];
}

- (void)repeatOccuredOnSplashScreen
{
    [self.view showActivityIndicaror];
    [self.interactorIn startLoadingRadioStations];
}

@end

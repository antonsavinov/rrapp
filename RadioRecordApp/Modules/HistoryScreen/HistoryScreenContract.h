//
//  HistoryScreenContract.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 28/02/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class HistoryViewController;

@class RadioStation;
@class HistorySong;

@protocol HistoryPresenter;
@protocol HistoryInteractorIn;
@protocol HistoryInteractorOut;

@protocol HistoryRouter <NSObject>

@property (nonatomic, weak) HistoryViewController* view;

+ (HistoryViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation;
- (void)returnToRadioPlayerScreen;

@end

@protocol HistoryView <NSObject>

@property (strong, nonatomic) id<HistoryPresenter>presenter;

- (void)fetchedHistoryDates:(NSArray<NSDate*>*)dates;
- (void)refreshSongs;
- (void)showEmptyView;
- (void)finishedPlayedSongAtIndex:(NSUInteger)index;
- (void)songDurationAvailableAtIndex:(NSUInteger)index;

@end

@protocol HistoryPresenter <NSObject>

@property (strong, nonatomic) id<HistoryInteractorIn>interactorIn;
@property (weak, nonatomic)   id<HistoryView>view;
@property (strong, nonatomic) id<HistoryRouter>router;

- (void)historyScreenLoaded;
- (void)bubleTappedByIndex:(NSUInteger)index;

- (void)backButtonTapped;
- (void)playButtonTappedByIndex:(NSUInteger)index;
- (void)pauseButtonTapped;

- (NSUInteger)songsCount;
- (HistorySong*)songAtIndex:(NSUInteger)index;
- (BOOL)songIsPlayingAtIndex:(NSUInteger)index;

@end

@protocol HistoryInteractorIn <NSObject>

@property (weak, nonatomic) id<HistoryInteractorOut>interactorOut;

- (void)requestHistoryDates;
- (void)requestHistorySongsForDate:(NSDate*)date;
- (BOOL)playerIsPlayingSongByURL:(NSString*)url;
- (void)startPlayingByURL:(NSString*)url;
- (void)stopPlaying;

@end

@protocol HistoryInteractorOut <NSObject>

- (void)fetchedDates:(NSArray<NSDate*>*)dates;
- (void)fetchedHistorySongs:(NSArray<HistorySong *>*)historySongs;
- (void)mediaPlayerFinishedPlaying;
- (void)durationLoadedForPlayingSong:(CGFloat)duration;

@end

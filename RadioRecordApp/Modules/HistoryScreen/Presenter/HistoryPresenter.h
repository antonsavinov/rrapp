//
//  HistoryPresenter.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryScreenContract.h"

@interface HistoryPresenter : NSObject <HistoryPresenter>

@property (strong, nonatomic) id<HistoryInteractorIn>interactorIn;
@property (weak, nonatomic)   id<HistoryView>view;
@property (strong, nonatomic) id<HistoryRouter>router;

@end

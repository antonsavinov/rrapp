//
//  HistoryPresenter.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryPresenter.h"
#import "HistorySong.h"

@interface HistoryPresenter () <HistoryInteractorOut>

@property (strong, nonatomic) NSArray<NSDate *>* dates;
@property (strong, nonatomic) NSArray<HistorySong *>* songs;

@property (assign, nonatomic) NSUInteger index;
@property (strong, nonatomic) NSNumber* lastPlayedIndex;

@end

@implementation HistoryPresenter

#pragma mark - HistoryPresenter

- (void)historyScreenLoaded {
    [self.interactorIn requestHistoryDates];
}

- (void)backButtonTapped {
    if (self.songs.count > 0) {
        if ([self.interactorIn playerIsPlayingSongByURL:[self.songs[self.index] remoteUrlForPlaying]]) {
            [self.interactorIn stopPlaying];
        }
    }
    [self.router returnToRadioPlayerScreen];
}

- (void)playButtonTappedByIndex:(NSUInteger)index {
    
    if (self.lastPlayedIndex) {
        [self.interactorIn stopPlaying];
        [self.view finishedPlayedSongAtIndex:self.lastPlayedIndex.unsignedIntegerValue];
    }
    
    if ([self.interactorIn playerIsPlayingSongByURL:[self.songs[self.index] remoteUrlForPlaying]]) {
        [self.interactorIn stopPlaying];
        [self.view finishedPlayedSongAtIndex:self.index];
    }
    
    self.index = index;
    [self.interactorIn startPlayingByURL:[self.songs[index] remoteUrlForPlaying]];
}

- (void)pauseButtonTapped {
    [self.interactorIn stopPlaying];
}

- (void)bubleTappedByIndex:(NSUInteger)index {
    [self.interactorIn requestHistorySongsForDate:self.dates[index]];
}

- (NSUInteger)songsCount {
    return self.songs.count;
}

- (HistorySong *)songAtIndex:(NSUInteger)index {
    return self.songs[index];
}

- (BOOL)songIsPlayingAtIndex:(NSUInteger)index {
    BOOL playerIsPlayingCurrentSongByIndex = [self.interactorIn playerIsPlayingSongByURL:[self.songs[index] remoteUrlForPlaying]];
    if (playerIsPlayingCurrentSongByIndex) {
        self.lastPlayedIndex = @(index);
    }
    return playerIsPlayingCurrentSongByIndex;
}

#pragma mark - InteractorOut

- (void)fetchedDates:(NSArray<NSDate *> *)dates {

    self.dates = dates;
    [self.view fetchedHistoryDates:dates];
}

- (void)fetchedHistorySongs:(NSArray *)historySongs {

    self.songs = historySongs;
    if (self.songs.count == 0) {
        [self.view showEmptyView];
        return;
    }
    [self.view refreshSongs];
}

- (void)durationLoadedForPlayingSong:(CGFloat)duration {
    HistorySong* song = self.songs[self.index];
    song.duration = duration;
    [self.view songDurationAvailableAtIndex:self.index];
}

- (void)mediaPlayerFinishedPlaying {
    [self.view finishedPlayedSongAtIndex:self.index];
}

@end

//
//  HistoryInteractor.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryScreenContract.h"

@interface HistoryInteractor : NSObject <HistoryInteractorIn>

@property (strong, nonatomic) RadioStation* radioStation;
@property (weak, nonatomic) id<HistoryInteractorOut>interactorOut;

@end

//
//  HistoryInteractor.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryInteractor.h"

#import "MediaPlayerManager.h"
#import "RRManager.h"

#import "Song.h"

//https://github.com/erica/NSDate-Extensions/

@interface HistoryInteractor () <MediaPlayerManagerDelegate>

@end

@implementation HistoryInteractor

- (void)requestHistoryDates {

    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.calendar = calendar;

    NSMutableArray<NSDate *>* dates = [NSMutableArray new];
    for (NSUInteger index = 0; index < 7; index++) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.day = index * (-1);
        NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        [dates addObject:newDate];
//        [dates insertObject:newDate atIndex:7 - 1 - index];
    }
    
    NSArray *reversed = [[dates reverseObjectEnumerator] allObjects];

    [self.interactorOut fetchedDates:reversed];
}

- (void)requestHistorySongsForDate:(NSDate *)date {
    [[RRManager sharedInstance] fetchSongsByDate:date
                                 forRadioStation:self.radioStation
                             withCompletionBlock:^(NSArray<HistorySong *> *historySongs) {
         [self.interactorOut fetchedHistorySongs:historySongs];
     }];
}

- (void)startPlayingByURL:(NSString *)url {
    BOOL isPlayingNow = [[MediaPlayerManager sharedInstance] isPlayingNow];
    if (isPlayingNow) {
        [[MediaPlayerManager sharedInstance] stopPlaying];
    }
    [[MediaPlayerManager sharedInstance] setDelegate:self];
    [[MediaPlayerManager sharedInstance] playMusicByURL:url];
}

- (void)stopPlaying {
    [[MediaPlayerManager sharedInstance] stopPlaying];
}

- (BOOL)playerIsPlayingSongByURL:(NSString *)url {
    return [[MediaPlayerManager sharedInstance] isAlreadyPlayingByURL:url];
}

#pragma mark - MediaPlayerManagerDelegate

- (void)streamInfoLoadedWithSong:(Song *)song {
    [self.interactorOut durationLoadedForPlayingSong:song.duration];
}

- (void)streamInfoNotLoadedWithError:(NSError *)error {
    
}

- (void)mediaPlayerFinishedPlaying {
    [self.interactorOut mediaPlayerFinishedPlaying];
}

- (void)mediaPlayerCanNotPlayRadioWithError:(NSError *)error {
    
}


@end

//
//  HistoryRouter.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryRouter.h"
#import "HistoryInteractor.h"
#import "HistoryPresenter.h"
#import "HistoryViewController.h"

@implementation HistoryRouter

+ (HistoryViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation {

    HistoryViewController* historyScreen = [HistoryViewController instanceFromStoryboard];

    HistoryPresenter* historyPresenter = [HistoryPresenter new];
    HistoryRouter* historyRouter = [HistoryRouter new];
    HistoryInteractor* historyInteractor = [HistoryInteractor new];

    historyScreen.presenter = historyPresenter;

    historyPresenter.interactorIn = historyInteractor;
    historyPresenter.view = historyScreen;
    historyPresenter.router = historyRouter;

    historyInteractor.interactorOut = historyPresenter;
    historyInteractor.radioStation = radioStation;

    historyRouter.view = historyScreen;

    return historyScreen;
}

- (void)returnToRadioPlayerScreen {
    UINavigationController* currentStack = self.view.navigationController;
    [currentStack popViewControllerAnimated:YES];
}


@end

//
//  HistoryViewController.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryScreenContract.h"

@interface HistoryViewController : UIViewController <HistoryView>

@property (strong, nonatomic) id<HistoryPresenter>presenter;

+ (HistoryViewController*)instanceFromStoryboard;

@end

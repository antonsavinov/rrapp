//
//  HistorySongTableViewCell.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 07/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemotedSongCellProtocol.h"
#import "HistorySongCellProtocol.h"

static NSString* const cellIdentifier = @"Song";

@protocol HistorySongCellDelegate;

@interface HistorySongCell : UITableViewCell <RemotedSongCellProtocol, HistorySongCellProtocol>

@property (weak, nonatomic) id<HistorySongCellDelegate>delegate;

+ (NSString*)identifier;

@end

@protocol HistorySongCellDelegate <NSObject>
- (void)historySongCell:(HistorySongCell*)cell playButtonChangedState:(BOOL)isPlaying;
@end

//
//  HistorySongCellProtocol.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 15/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HistorySongCellProtocol <NSObject>
- (void)setStartTime:(NSString*)startTime;
@end

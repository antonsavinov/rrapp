//
//  HistorySongTableViewCell.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 07/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistorySongCell.h"
#import "ASScrollableLabel.h"

@interface HistorySongCell ()

@property (weak, nonatomic) IBOutlet ASScrollableLabel *songTitleLabel;
@property (weak, nonatomic) IBOutlet ASScrollableLabel *artistTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tickTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (assign, nonatomic) BOOL isPlaying;

@end

@implementation HistorySongCell

#pragma mark - Lyfe Cycle

#pragma mark - Public

+ (NSString *)identifier {
    return @"historySongCellIdentifier";
}

- (void)setArtistName:(NSString *)artistName {
    self.artistTitleLabel.text = artistName;
    [self.artistTitleLabel scrollTextIfNeeded];
}

- (void)setSongTitle:(NSString *)songTitle {
    self.songTitleLabel.text = songTitle;
    [self.songTitleLabel scrollTextIfNeeded];
}

- (void)setStartTime:(NSString*)startTime {
    self.startTimeLabel.text = startTime;
}

- (void)setDuration:(CGFloat)duration {
    self.tickTimeLabel.text = duration == 0 ? @"" : [self durationFormatted:duration];
}

- (void)setPlayState:(BOOL)isPlaying {
    self.isPlaying = isPlaying;
    [self updatePlayIcon];
}

#pragma mark - Private

- (void)updatePlayIcon {
    if (self.isPlaying) {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"pause-icon"] forState:UIControlStateNormal];
    } else {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"play-icon"] forState:UIControlStateNormal];
    }
}

- (NSString *)durationFormatted:(CGFloat)durationInSecconds {
    NSUInteger roundSeconds = (NSUInteger)(round(durationInSecconds));
    NSUInteger seconds = roundSeconds % 60;
    NSUInteger minutes = (roundSeconds / 60) % 60;
    NSUInteger hours = durationInSecconds / 3600;
    if (hours > 0) {
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    } else {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }
}

#pragma mark - IBActions

- (IBAction)playButtonTappedAction:(UIButton *)sender {
    self.isPlaying = !self.isPlaying;
    
    [self updatePlayIcon];
    [self.delegate historySongCell:self playButtonChangedState:self.isPlaying];
}

@end

//
//  HistoryViewController.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 02/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "HistoryViewController.h"

#import "NavigationBarView.h"
#import "BublesView.h"
#import "RemotedSongListView.h"

#import "UIColor+Utils.h"

@interface HistoryViewController () <NavigationBarViewDelegate, BublesViewDelegate, RemotedSongListViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet NavigationBarView *navigationBar;
@property (weak, nonatomic) IBOutlet BublesView *bublesView;
@property (weak, nonatomic) IBOutlet RemotedSongListView *remotedSongListView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bublesViewTopPaddingConstraint;

//@property (nonatomic, assign) CGFloat lastContentOffset;

@end

@implementation HistoryViewController

+ (HistoryViewController*)instanceFromStoryboard {

    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HistoryViewController* historyScreen =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"historyViewControllerIdentifier"];

    return historyScreen;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
    [self.presenter historyScreenLoaded];
}

#pragma mark - Private

- (void)setupUI {
    self.navigationBar.titleLabel.text = @"History";
    self.navigationBar.delegate = self;
    self.navigationBar.leftBarItem = NavigationBarItemBack;

    self.bublesView.delegate = self;
    [self.bublesView setUIAttributes:@{kBublesViewBackgroundColor : [UIColor radioRecordWhiteColor],
                                       kBubleViewBackgroundColor : [UIColor radioRecordGrayColor],
                                       kBubleViewFontColor : [UIColor whiteColor],
                                       kBubleViewTouchedBackgroundColor : [UIColor radioRecordDarkPinColor],
                                       kBubleViewTouchedFontColor : [UIColor whiteColor],
                                       kBubleViewFont : [UIFont fontWithName:@"EuropeExt" size:18]
                                      }];

    self.remotedSongListView.listType = RemotedSongListTypeHistory;
    self.remotedSongListView.delegate = self;

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - NavigationBarView

- (void)navigationBarView:(NavigationBarView *)navigationBarView wasClickedLeftItem:(UIButton *)leftButton {
    [self.presenter backButtonTapped];
}

#pragma mark - HistoryView

- (void)fetchedHistoryDates:(NSArray<NSDate *> *)dates {

    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"d MMMM";
    dateFormatter.calendar = calendar;

    NSMutableArray<NSString *>* datesString = [NSMutableArray new];
    for (NSDate *date in dates) {
        [datesString addObject:[dateFormatter stringFromDate:date]];
    }

    [self.remotedSongListView setMessage:@"Choose date"];

    [self.bublesView setTitles:datesString];
    [self.bublesView apply];
}

- (void)refreshSongs {
    [self.remotedSongListView refreshSongs];
}

- (void)showEmptyView {
    [self.remotedSongListView showEmptyView];
}

- (void)finishedPlayedSongAtIndex:(NSUInteger)index {
    [self.remotedSongListView songFinishedPlayingAtIndex:index];
}

- (void)songDurationAvailableAtIndex:(NSUInteger)index {
    [self.remotedSongListView updateSongDurationAtIndex:index];
}

#pragma mark - BublesViewDelegate

- (void)bubleTappedByIndex:(NSUInteger)index {
    [self.remotedSongListView showLoadingScreen];
    [self.presenter bubleTappedByIndex:index];
}

#pragma mark - RemotedSongListViewDelegate

- (NSUInteger)numberOfSongs {
    return [self.presenter songsCount];
}

- (id<RemotedSongProtocol, HistorySongProtocol>)songAtIndex:(NSUInteger)index {
    return [self.presenter songAtIndex:index];
}

- (BOOL)isSongPlayedAtIndex:(NSUInteger)index {
    return [self.presenter songIsPlayingAtIndex:index];
}

- (void)songTappedForPlayingByIndex:(NSUInteger)index {
    [self.presenter playButtonTappedByIndex:index];
}

- (void)songTappedForStopping {
    [self.presenter pauseButtonTapped];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < 0) {
        return;
    }
    
//    RRLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
//    
//    if (scrollView.contentOffset.y - self.lastContentOffset < 0 && scrollView.contentOffset.y < 80) {
//        [self.bublesView layoutIfNeeded];
//        self.bublesViewTopPaddingConstraint.constant = (-1)*scrollView.contentOffset.y;
//        [self.bublesView layoutIfNeeded];
//    } else if (scrollView.contentOffset.y - self.lastContentOffset > 0 && scrollView.contentOffset.y < 80) {
//        [self.bublesView layoutIfNeeded];
//        self.bublesViewTopPaddingConstraint.constant =- scrollView.contentOffset.y;
//        [self.bublesView layoutIfNeeded];
//    }
//    self.lastContentOffset = scrollView.contentOffset.y;
}

@end

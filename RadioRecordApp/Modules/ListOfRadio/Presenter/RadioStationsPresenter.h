//
//  RadioStationsPresenter.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioStationsScreenContract.h"

@interface RadioStationsPresenter : NSObject <RadioStationsPresenter>

@property (strong, nonatomic) id<RadioStationsInteractorIn>interactorIn;
@property (weak, nonatomic)   id<RadioStationsView>view;
@property (strong, nonatomic) id<RadioStationsRouter>router;

@end

//
//  RadioStationsPresenter.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioStationsPresenter.h"

@implementation RadioStationsPresenter

#pragma mark - RadioStationsPresenter

- (void)didSelectRadioStation:(RadioStation*)radioStation
{
    [self.router presentRadioStationScreenForCurrentStation:radioStation];
}

@end

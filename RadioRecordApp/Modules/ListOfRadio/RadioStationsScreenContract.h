//
//  RadioStationsScreen.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class RadioStationsViewController;
@class RadioStation;

@protocol RadioStationsPresenter;
@protocol RadioStationsInteractorIn;
@protocol RadioStationsInteractorOut;
@protocol RadioStationsRouter;
@protocol RadioStationsView;

@protocol RadioStationsRouter <NSObject>

+ (RadioStationsViewController*)assembleModuleForRadioStations:(NSArray<RadioStation*>*)radioStations;
- (void)presentRadioStationScreenForCurrentStation:(RadioStation*)radioStation;

@end

@protocol RadioStationsView <NSObject>

@property (strong, nonatomic) id<RadioStationsPresenter>presenter;

@end

@protocol RadioStationsPresenter <NSObject>

@property (strong, nonatomic) id<RadioStationsInteractorIn>interactorIn;
@property (weak, nonatomic)   id<RadioStationsView>view;
@property (strong, nonatomic) id<RadioStationsRouter>router;

- (void)didSelectRadioStation:(RadioStation*)radioStation;

@end

@protocol RadioStationsInteractorIn <NSObject>

@property (weak, nonatomic) id<RadioStationsInteractorOut>interactorOut;

@end

@protocol RadioStationsInteractorOut <NSObject>

@end
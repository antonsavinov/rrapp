//
//  RadioStationsRouter.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioStationsRouter.h"
#import "RadioStationsPresenter.h"
#import "RadioStationsRouter.h"
#import "RadioStationsInteractor.h"

#import "RadioPlayerRouter.h"

#import "RadioStationsViewController.h"
#import "RadioPlayerViewController.h"

@implementation RadioStationsRouter

#pragma mark - RadioStationsRouter

+ (RadioStationsViewController*)assembleModuleForRadioStations:(NSArray<RadioStation*>*)radioStations {
    
    RadioStationsViewController* radioStationsScreen = [RadioStationsViewController instanceFromStoryboard];
    radioStationsScreen.radioStations = radioStations;
    
    RadioStationsPresenter* radioStationsPresenter = [RadioStationsPresenter new];
    RadioStationsRouter* radioStationsRouter = [RadioStationsRouter new];
    RadioStationsInteractor* radioStationsInteractor = [RadioStationsInteractor new];
    
    radioStationsScreen.presenter = radioStationsPresenter;
    
    radioStationsPresenter.interactorIn = radioStationsInteractor;
    radioStationsPresenter.view = radioStationsScreen;
    radioStationsPresenter.router = radioStationsRouter;
    
    radioStationsInteractor.interactorOut = radioStationsPresenter;
    
    radioStationsRouter.view = radioStationsScreen;
    
    return radioStationsScreen;
}

- (void)presentRadioStationScreenForCurrentStation:(RadioStation*)radioStation
{
    RadioPlayerViewController* radioPlayerViewController =
    [RadioPlayerRouter assembleModuleForRadioStation:radioStation];
    
    UINavigationController* currentStack = self.view.navigationController;
    [currentStack pushViewController:radioPlayerViewController animated:YES];
}

@end

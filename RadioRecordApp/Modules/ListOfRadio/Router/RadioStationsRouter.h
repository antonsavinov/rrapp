//
//  RadioStationsRouter.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioStationsScreenContract.h"

@interface RadioStationsRouter : NSObject <RadioStationsRouter>

@property (nonatomic, weak) RadioStationsViewController* view;

@end

//
//  RadioStationsInteractor.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioStationsScreenContract.h"

@interface RadioStationsInteractor : NSObject <RadioStationsInteractorIn>

@property (nonatomic, weak) id<RadioStationsInteractorOut>interactorOut;

@end

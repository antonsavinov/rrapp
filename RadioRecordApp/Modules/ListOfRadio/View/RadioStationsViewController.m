//
//  MainScreenViewController.m
//  RadioRecordApp
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RadioStationsViewController.h"
#import "RadioPlayerViewController.h"

#import "NavigationBarView.h"
#import "RadioTableViewCell.h"

#import "RRManager.h"

@interface RadioStationsViewController () <NavigationBarViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet NavigationBarView *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation RadioStationsViewController

# pragma mark - Life cycle

+ (RadioStationsViewController*)instanceFromStoryboard {
    
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RadioStationsViewController* radioStationsScreen =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"radioStationsScreenIdentifier"];
    
    return radioStationsScreen;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)setupUI
{
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.radioStations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"radioStationCellIdentifier"
                                                               forIndexPath:indexPath];
    if (!cell) return [[UITableViewCell alloc] init];
    
    [cell setupCellForRadioStation:self.radioStations[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.presenter didSelectRadioStation:self.radioStations[indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

@end

//
//  MainScreenViewController.h
//  RadioRecordApp
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RadioStationsScreenContract.h"

@class RadioStation;

@interface RadioStationsViewController : UIViewController <RadioStationsView>

@property (strong, nonatomic) NSArray<RadioStation*>* radioStations;
@property (strong, nonatomic) id<RadioStationsPresenter>presenter;

+ (RadioStationsViewController*)instanceFromStoryboard;

@end

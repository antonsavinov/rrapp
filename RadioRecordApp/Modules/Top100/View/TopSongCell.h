//
//  TopSongCell.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemotedSongCellProtocol.h"

@protocol TopSongCellDelegate;

@interface TopSongCell : UITableViewCell <RemotedSongCellProtocol>

@property (weak, nonatomic) id<TopSongCellDelegate>delegate;

+ (NSString *)identifier;

@end

@protocol TopSongCellDelegate <NSObject>

- (void)topSongCell:(TopSongCell*)cell playButtonChangedState:(BOOL)isPlaying;

@end

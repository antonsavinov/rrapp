//
//  TopSongCell.m
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "TopSongCell.h"
#import "ASScrollableLabel.h"

@interface TopSongCell ()

@property (weak, nonatomic) IBOutlet ASScrollableLabel *songLabel;
@property (weak, nonatomic) IBOutlet ASScrollableLabel *artistLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (assign, nonatomic) BOOL isPlaying;

@end

@implementation TopSongCell

#pragma mark - Lyfe Cycle

#pragma mark - Public

+ (NSString *)identifier {
    return @"topSongCellIdentifier";
}

- (void)setArtistName:(NSString *)artistName {
    self.artistLabel.text = artistName;
    [self.artistLabel scrollTextIfNeeded];
}

- (void)setSongTitle:(NSString *)songTitle {
    self.songLabel.text = songTitle;
    [self.songLabel scrollTextIfNeeded];
}

- (void)setPlayState:(BOOL)isPlaying {
    self.isPlaying = isPlaying;
    
    [self updatePlayIcon];
}

#pragma mark - Private

- (void)updatePlayIcon {
    if (self.isPlaying) {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"pause-icon"] forState:UIControlStateNormal];
    } else {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"play-icon"] forState:UIControlStateNormal];
    }
}

#pragma mark - IBActions

- (IBAction)playButtonAction:(id)sender {
    self.isPlaying = !self.isPlaying;
    
    [self updatePlayIcon];
    
    [self.delegate topSongCell:self playButtonChangedState:self.isPlaying];
}

@end

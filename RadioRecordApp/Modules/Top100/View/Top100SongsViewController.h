//
//  Top100SongsViewController.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100ScreenContract.h"

@interface Top100SongsViewController : UIViewController <Top100SongsView>

@property (strong, nonatomic) id<Top100SongsPresenter>presenter;

+ (Top100SongsViewController*)instanceFromStoryboard;

@end

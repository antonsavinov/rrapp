//
//  Top100SongsViewController.m
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100SongsViewController.h"
#import "NavigationBarView.h"

#import "RemotedSongListView.h"

@interface Top100SongsViewController () <NavigationBarViewDelegate, RemotedSongListViewDelegate>

@property (weak, nonatomic) IBOutlet NavigationBarView *navigationBarView;
@property (weak, nonatomic) IBOutlet RemotedSongListView *remotedSongListView;

@end

@implementation Top100SongsViewController

#pragma mark - Lyfe Cycle

+ (Top100SongsViewController *)instanceFromStoryboard {
    
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    Top100SongsViewController* top100SongsScreen =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"top100SongsViewControllerIdentifier"];
    
    return top100SongsScreen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter top100SongsScreenLoaded];
    [self setupUI];
}

#pragma mark - Private

- (void)setupUI {
    self.navigationBarView.titleLabel.text = @"Top 100 Songs";
    self.navigationBarView.delegate = self;
    self.navigationBarView.leftBarItem = NavigationBarItemBack;

    self.remotedSongListView.listType = RemotedSongListTypeHistory;
    self.remotedSongListView.delegate = self;
    [self.remotedSongListView showLoadingScreen];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - RemotedSongListViewDelegate

- (NSUInteger)numberOfSongs {
    return [self.presenter songsCount];
}

- (id<RemotedSongProtocol, HistorySongProtocol>)songAtIndex:(NSUInteger)index {
    return [self.presenter songAtIndex:index];
}

- (BOOL)isSongPlayedAtIndex:(NSUInteger)index {
    return [self.presenter songIsPlayingAtIndex:index];
}

- (void)songTappedForPlayingByIndex:(NSUInteger)index {
    [self.presenter playButtonTappedByIndex:index];
}

- (void)songTappedForStopping {
    [self.presenter pauseButtonTapped];
}

#pragma mark - NavigationBarView

- (void)navigationBarView:(NavigationBarView *)navigationBarView wasClickedLeftItem:(UIButton *)leftButton {
    [self.presenter backButtonTapped];
}

#pragma mark - Top100SongsView

- (void)refreshSongs {
    [self.remotedSongListView refreshSongs];
}

- (void)finishedPlayedSongAtIndex:(NSUInteger)index {
    [self.remotedSongListView songFinishedPlayingAtIndex:index];
}

- (void)showEmptyView {
    [self.remotedSongListView showEmptyView];
}

@end

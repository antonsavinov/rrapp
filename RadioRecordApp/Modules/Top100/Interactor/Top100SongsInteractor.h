//
//  Top100Interactor.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100ScreenContract.h"

@interface Top100SongsInteractor : NSObject <Top100SongsInteractorIn>

@property (strong, nonatomic) RadioStation* radioStation;
@property (weak, nonatomic) id<Top100SongsInteractorOut>interactorOut;

@end

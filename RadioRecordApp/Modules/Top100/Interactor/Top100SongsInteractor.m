//
//  Top100Interactor.m
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100SongsInteractor.h"

#import "RRManager.h"
#import "MediaPlayerManager.h"

@interface Top100SongsInteractor () <MediaPlayerManagerDelegate>

@end

@implementation Top100SongsInteractor

- (void)requestTop100Songs {
    [[RRManager sharedInstance] fetchTop100SongsForRadioStation:self.radioStation
                                            withCompletionBlock:^(NSArray<TopSong *> *topSongs) {
                                                [self.interactorOut top100SongsFetched:topSongs];
    }];
}

- (void)startPlayingByURL:(NSString *)url {
    BOOL isPlayingNow = [[MediaPlayerManager sharedInstance] isPlayingNow];
    if (isPlayingNow) {
        [[MediaPlayerManager sharedInstance] stopPlaying];
    }
    [[MediaPlayerManager sharedInstance] setDelegate:self];
    [[MediaPlayerManager sharedInstance] playMusicByURL:url];
}

- (void)stopPlaying {
    [[MediaPlayerManager sharedInstance] stopPlaying];
}

- (BOOL)playerIsPlayingSongByURL:(NSString *)url {
    return [[MediaPlayerManager sharedInstance] isAlreadyPlayingByURL:url];
}

#pragma mark - MediaPlayerManagerDelegate

- (void)mediaPlayerFinishedPlaying {
    [self.interactorOut mediaPlayerFinishedPlaying];
}

- (void)mediaPlayerCanNotPlayRadioWithError:(NSError *)error {
    
}

- (void)streamInfoLoadedWithSong:(Song*)song {
    
}

- (void)streamInfoNotLoadedWithError:(NSError*)error {
    
}

@end

//
//  Top100ScreenContract.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Top100SongsViewController;
@class TopSong;
@class RadioStation;

@protocol Top100SongsPresenter;
@protocol Top100SongsInteractorIn;
@protocol Top100SongsInteractorOut;
@protocol Top100SongsPresenter;

@protocol Top100SongsRouter <NSObject>

@property (nonatomic, weak) Top100SongsViewController* view;

+ (Top100SongsViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation;
- (void)returnToRadioPlayerScreen;

@end

@protocol Top100SongsView <NSObject>

@property (strong, nonatomic) id<Top100SongsPresenter>presenter;

- (void)refreshSongs;
- (void)finishedPlayedSongAtIndex:(NSUInteger)index;
- (void)showEmptyView;

@end

@protocol Top100SongsPresenter <NSObject>

@property (strong, nonatomic) id<Top100SongsInteractorIn>interactorIn;
@property (weak, nonatomic)   id<Top100SongsView>view;
@property (strong, nonatomic) id<Top100SongsRouter>router;

- (void)top100SongsScreenLoaded;
- (void)backButtonTapped;
- (void)playButtonTappedByIndex:(NSUInteger)index;
- (void)pauseButtonTapped;

- (NSUInteger)songsCount;
- (TopSong *)songAtIndex:(NSUInteger)index;
- (BOOL)songIsPlayingAtIndex:(NSUInteger)index;

@end

@protocol Top100SongsInteractorIn <NSObject>

@property (weak, nonatomic) id<Top100SongsInteractorOut>interactorOut;

- (void)requestTop100Songs;
- (BOOL)playerIsPlayingSongByURL:(NSString*)url;
- (void)startPlayingByURL:(NSString*)url;
- (void)stopPlaying;

@end

@protocol Top100SongsInteractorOut <NSObject>

- (void)top100SongsFetched:(NSArray<TopSong*>*)top100Songs;
- (void)mediaPlayerFinishedPlaying;

@end

//
//  Top100Presenter.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100ScreenContract.h"

@interface Top100SongsPresenter : NSObject <Top100SongsPresenter>

@property (strong, nonatomic) id<Top100SongsInteractorIn>interactorIn;
@property (weak, nonatomic)   id<Top100SongsView>view;
@property (strong, nonatomic) id<Top100SongsRouter>router;

@end

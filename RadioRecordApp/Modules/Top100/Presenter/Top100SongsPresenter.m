//
//  Top100Presenter.m
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100SongsPresenter.h"
#import "TopSong.h"

@interface Top100SongsPresenter () <Top100SongsInteractorOut>

@property (strong, nonatomic) NSArray<TopSong*>* top100Songs;
@property (assign, nonatomic) NSUInteger index;
@property (strong, nonatomic) NSNumber* lastPlayedIndex;

@end

@implementation Top100SongsPresenter

#pragma mark - Top100SongsPresenter

- (void)top100SongsScreenLoaded {
    [self.interactorIn requestTop100Songs];
}

- (void)backButtonTapped {

    if (self.top100Songs.count > 0) {
        if ([self.interactorIn playerIsPlayingSongByURL:[self.top100Songs[self.index] remoteUrlForPlaying]]) {
            [self.interactorIn stopPlaying];
        }
    }

    [self.router returnToRadioPlayerScreen];
}

- (void)playButtonTappedByIndex:(NSUInteger)index {

    if (self.lastPlayedIndex) {
        [self.interactorIn stopPlaying];
        [self.view finishedPlayedSongAtIndex:self.lastPlayedIndex.unsignedIntegerValue];
    }
    
    if ([self.interactorIn playerIsPlayingSongByURL:[self.top100Songs[self.index] remoteUrlForPlaying]]) {
        [self.interactorIn stopPlaying];
        [self.view finishedPlayedSongAtIndex:self.index];
    }

    self.index = index;
    [self.interactorIn startPlayingByURL:[self.top100Songs[index] remoteUrlForPlaying]];
}

- (void)pauseButtonTapped {
    [self.interactorIn stopPlaying];
}

- (NSUInteger)songsCount {
    return self.top100Songs.count;
}
- (TopSong *)songAtIndex:(NSUInteger)index {
    return self.top100Songs[index];
}

- (BOOL)songIsPlayingAtIndex:(NSUInteger)index {
    BOOL playerIsPlayingCurrentSongByIndex = [self.interactorIn playerIsPlayingSongByURL:[self.top100Songs[index] remoteUrlForPlaying]];
    if (playerIsPlayingCurrentSongByIndex) {
        self.lastPlayedIndex = @(index);
    }
    return playerIsPlayingCurrentSongByIndex;
}

#pragma mark - Top100SongsInteractorOut

- (void)top100SongsFetched:(NSArray<TopSong *> *)top100Songs {
    self.top100Songs = top100Songs;
    if (self.top100Songs.count == 0) {
        [self.view showEmptyView];
        return;
    }
    [self.view refreshSongs];
}

- (void)mediaPlayerFinishedPlaying {
    [self.view finishedPlayedSongAtIndex:self.index];
}

@end

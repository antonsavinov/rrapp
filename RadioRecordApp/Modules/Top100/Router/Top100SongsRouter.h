//
//  Top100Router.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100ScreenContract.h"

@interface Top100SongsRouter : NSObject <Top100SongsRouter>

@property (nonatomic, weak) Top100SongsViewController* view;

@end

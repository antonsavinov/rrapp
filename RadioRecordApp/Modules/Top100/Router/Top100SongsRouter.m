//
//  Top100Router.m
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "Top100SongsRouter.h"
#import "Top100SongsInteractor.h"
#import "Top100SongsPresenter.h"

#import "Top100SongsViewController.h"

@implementation Top100SongsRouter

+ (Top100SongsViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation {
    
    Top100SongsViewController* top100SongsScreen = [Top100SongsViewController instanceFromStoryboard];
    
    Top100SongsPresenter* top100Presenter = [Top100SongsPresenter new];
    Top100SongsRouter* top100SongsRouter = [Top100SongsRouter new];
    Top100SongsInteractor* top100SongsInteractor = [Top100SongsInteractor new];
    
    top100SongsScreen.presenter = top100Presenter;
    
    top100Presenter.interactorIn = top100SongsInteractor;
    top100Presenter.view = top100SongsScreen;
    top100Presenter.router = top100SongsRouter;
    
    top100SongsInteractor.interactorOut = top100Presenter;
    top100SongsInteractor.radioStation = radioStation;
    
    top100SongsRouter.view = top100SongsScreen;
    
    return top100SongsScreen;
}

- (void)returnToRadioPlayerScreen {
    UINavigationController* currentStack = self.view.navigationController;
    [currentStack popViewControllerAnimated:YES];
}

@end

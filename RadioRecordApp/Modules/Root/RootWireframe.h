//
//  RootWireframe.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RootWireframeProtocol.h"

@interface RootWireframe : NSObject <RootWireframeProtocol>

@end

//
//  RootWireframeProtocol.h
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol RootWireframeProtocol <NSObject>

- (void)presentSplashScreenForWindow:(UIWindow*)window;

@end

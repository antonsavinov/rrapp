//
//  RootWireframe.m
//  RadioRecordApp
//
//  Created by Admin on 04.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RootWireframe.h"
#import "SplashScreenRouter.h"

@implementation RootWireframe

- (void)presentSplashScreenForWindow:(UIWindow*)window
{
    window.rootViewController = [SplashScreenRouter assembleModule];
    [window makeKeyAndVisible];
}

@end

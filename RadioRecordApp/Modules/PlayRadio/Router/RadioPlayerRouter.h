//
//  RadioPlayerScreenRouter.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerScreenContract.h"

@interface RadioPlayerRouter : NSObject <RadioPlayerRouter>

@property (nonatomic, weak) RadioPlayerViewController* view;

@end

//
//  RadioPlayerScreenRouter.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerRouter.h"
#import "RadioPlayerPresenter.h"
#import "RadioPlayerInteractor.h"

#import "RadioPlayerViewController.h"

#import "Top100SongsRouter.h"
#import "Top100SongsViewController.h"

#import "HistoryRouter.h"
#import "HistoryViewController.h"

@implementation RadioPlayerRouter

#pragma mark - Life cycle

- (void)dealloc
{
    RRLog(@"RadioPlayerRouter dismissed");
}

#pragma mark - RadioPlayerRouter

+ (RadioPlayerViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation
{
    RadioPlayerViewController* radioPlayerScreen = [RadioPlayerViewController instanceFromStoryboard];
    
    RadioPlayerPresenter* radioPlayerPresenter = [RadioPlayerPresenter new];
    RadioPlayerRouter* radioPlayerRouter = [RadioPlayerRouter new];
    RadioPlayerInteractor* radioPlayerInteractor = [RadioPlayerInteractor new];
    
    radioPlayerScreen.presenter = radioPlayerPresenter;
    
    radioPlayerPresenter.interactorIn = radioPlayerInteractor;
    radioPlayerPresenter.view = radioPlayerScreen;
    radioPlayerPresenter.router = radioPlayerRouter;
    
    radioPlayerInteractor.interactorOut = radioPlayerPresenter;
    radioPlayerInteractor.currentRadioStation = radioStation;
    
    radioPlayerRouter.view = radioPlayerScreen;
    
    return radioPlayerScreen;
}

- (void)launchiTunesAppByURL:(NSString *)url {
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
#else
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
#endif
}

- (void)presentTop100SongsScreenForCurrentStation:(RadioStation *)radioStation {
    
    Top100SongsViewController* top100SongViewController =
    [Top100SongsRouter assembleModuleForRadioStation:radioStation];
    
    UINavigationController* currentStack = self.view.navigationController;
    [currentStack pushViewController:top100SongViewController animated:YES];
}

- (void)presentHistoryScreenForCurrentStation:(RadioStation*)radioStation {

    HistoryViewController* historyViewController =
    [HistoryRouter assembleModuleForRadioStation:radioStation];

    UINavigationController* currentStack = self.view.navigationController;
    [currentStack pushViewController:historyViewController animated:YES];
}

@end

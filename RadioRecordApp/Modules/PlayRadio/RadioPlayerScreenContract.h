//
//  RadioPlayerScreenContract.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "RadioStation.h"
#import "RadioPlayerControlPanelView.h"

@class RadioPlayerViewController;
@class Song;

@protocol RadioPlayerRouter;
@protocol RadioPlayerInteractorIn;
@protocol RadioPlayerInteractorOut;
@protocol RadioPlayerPresenter;
@protocol RadioPlayerView;

@protocol RadioPlayerRouter <NSObject>

@property (nonatomic, weak) RadioPlayerViewController* view;

+ (RadioPlayerViewController*)assembleModuleForRadioStation:(RadioStation*)radioStation;
- (void)launchiTunesAppByURL:(NSString *)url;
- (void)presentTop100SongsScreenForCurrentStation:(RadioStation*)radioStation;
- (void)presentHistoryScreenForCurrentStation:(RadioStation*)radioStation;

@end

@protocol RadioPlayerView <NSObject>

@property (strong, nonatomic) id<RadioPlayerPresenter>presenter;

- (void)showInfoForSong:(Song*)song;
- (void)showAlbumImage:(UIImage*)image;
- (void)showLoading;
- (void)showPause;
- (void)showStart;
- (void)prepareRadioPlayerScreenWithAudioQuality:(ControlPanelAudioQuality)audioQuality;
- (void)showError:(NSError*)error;
- (void)prepareRadioPlayerScreenWithVolume:(CGFloat)volume;

@end

@protocol RadioPlayerPresenter <NSObject>

@property (strong, nonatomic) id<RadioPlayerInteractorIn>interactorIn;
@property (weak, nonatomic)   id<RadioPlayerView>view;
@property (strong, nonatomic) id<RadioPlayerRouter>router;

- (void)requestRadioPlayerScreenConfiguration;
- (void)radioPlayerScreenDismissed;
- (void)radioPlayerScreenLoadedToPlayCurrentRadioStationWithAudioQuality:(ControlPanelAudioQuality)audioQuality;
- (void)startPlayingCurrentRadioStationWithAudioQuality:(ControlPanelAudioQuality)audioQuality;
- (void)stopPlayingRadio;
- (void)updatedAudioQuality:(ControlPanelAudioQuality)audioQuality;
- (void)top100SongsTapped;
- (void)historyTapped;
- (void)itunesImageTapped;

@end

@protocol RadioPlayerInteractorIn <NSObject>

@property (weak, nonatomic) id<RadioPlayerInteractorOut>interactorOut;

- (void)checkStoredAudioStreamTypeAndQuality;
- (void)checkVolume;

- (void)startStreamRadioImmediatelyifCanWithAudioStreamType:(RadioStationStreamType)audioStreamType
                                                 andQuality:(RadioStationQualityType)quality;
- (void)startStreamRadioWithAudioStreamType:(RadioStationStreamType)audioStreamType
                                 andQuality:(RadioStationQualityType)quality;
- (void)isPlayingNow;
- (void)stopStreamRadio;
- (void)storeAudioStreamType:(RadioStationStreamType)audioType;
- (void)storeAudioQualityType:(RadioStationQualityType)qualityType;
- (NSString *)iTunesURL;
- (RadioStation*)requestCurrentRadioStation;

@end

@protocol RadioPlayerInteractorOut <NSObject>

- (void)playerWillStream;
- (void)playerBeganStream;
- (void)playerStoppedStream;
- (void)becomeAvailableSong:(Song*)song;
- (void)isPlaying:(BOOL)isPlaying;
- (void)becomeAvailableAlbumImage:(UIImage*)image;
- (void)storedAudioStream:(RadioStationStreamType)audioStream
               andQuality:(RadioStationQualityType)quality;
- (void)playerCanNotPlayStreamWithError:(NSError*)error;
- (void)currentVolume:(CGFloat)volume;

@end

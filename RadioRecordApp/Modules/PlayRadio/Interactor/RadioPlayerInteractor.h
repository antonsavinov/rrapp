//
//  RadioPlayerScreenInteractor.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerScreenContract.h"
@class RadioStation;

@interface RadioPlayerInteractor : NSObject <RadioPlayerInteractorIn>

@property (strong, nonatomic) RadioStation* currentRadioStation;
@property (weak, nonatomic) id<RadioPlayerInteractorOut>interactorOut;

@end

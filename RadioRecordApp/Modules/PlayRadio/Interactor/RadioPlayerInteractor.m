//
//  RadioPlayerScreenInteractor.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerInteractor.h"
#import "MediaPlayerManager.h"
#import "iTunesSearchManager.h"
#import "ImageManager.h"

#import "SongLocalStorage.h"

#import "Song.h"

static NSString* const kStoredAudioStreamType   = @"kStoredAudioStreamType";
static NSString* const kStoredAudioQualityType  = @"kStoredAudioQualityType";

@interface RadioPlayerInteractor () <MediaPlayerManagerDelegate, iTunesSearchManagerDelegate, ImageManagerDelegate>

@property (nonatomic, strong) iTunesSearchManager* iTunesSearchManager;
@property (nonatomic, strong) ImageManager* imageManager;
@property (nonatomic, strong) Song* song;

@end

@implementation RadioPlayerInteractor

#pragma mark - Life cycle

- (instancetype)init {
    if (self = [super init]) {
        _iTunesSearchManager = [iTunesSearchManager new];
        _imageManager = [ImageManager new];
    }
    
    return self;
}

- (void)dealloc
{
    RRLog(@"RadioPlayerInteractor dismissed");
}

#pragma mark - Private

- (void)startPlayRadioByURL:(NSString*)url {
    self.song = nil;

    if([[MediaPlayerManager sharedInstance] isPlayingNow]) {
        [[MediaPlayerManager sharedInstance] stopPlaying];
    }

    [[MediaPlayerManager sharedInstance] setDelegate:self];
    [[MediaPlayerManager sharedInstance] playMusicByURL:url];

    //TODO: A.S.: Added new delegate to show loading icon
    [self.interactorOut playerWillStream];
}

- (void)stopPlayRadio {
    
    [[MediaPlayerManager sharedInstance] stopPlaying];
    
    [self.interactorOut playerStoppedStream];
}

#pragma mark - RadioPlayerInteractorIn

- (void)checkStoredAudioStreamTypeAndQuality {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    RadioStationStreamType streamType = [defaults integerForKey:kStoredAudioStreamType];
    RadioStationQualityType qualityType = [defaults integerForKey:kStoredAudioQualityType];
    
    [self.interactorOut storedAudioStream:streamType andQuality:qualityType];
}

- (void)checkVolume {
    CGFloat volume = [[MediaPlayerManager sharedInstance] volume];
    [self.interactorOut currentVolume:volume];
}

- (void)startStreamRadioImmediatelyifCanWithAudioStreamType:(RadioStationStreamType)audioStreamType andQuality:(RadioStationQualityType)quality {
    NSString* streamedURL = [self.currentRadioStation composeURLWithStreamAudioType:audioStreamType
                                                                     andQualityType:quality];
    BOOL isPlayingNow = [[MediaPlayerManager sharedInstance] isPlayingNow];
    if (isPlayingNow) {
        BOOL isStreamed = [[MediaPlayerManager sharedInstance] isAlreadyPlayingByURL:streamedURL];
        if (isStreamed) {
            [[MediaPlayerManager sharedInstance] setDelegate:self];
            [[MediaPlayerManager sharedInstance] restartObservingStreamInfo];
            [self.interactorOut isPlaying:YES];
        } else {
            [self stopPlayRadio];
            [self startPlayRadioByURL:streamedURL];
        }
        
    } else {
        [self.interactorOut isPlaying:NO];
    }
}

- (void)startStreamRadioWithAudioStreamType:(RadioStationStreamType)audioStreamType andQuality:(RadioStationQualityType)quality {
    NSString* streamedURL = [self.currentRadioStation composeURLWithStreamAudioType:audioStreamType
                                                                  andQualityType:quality];
    [self startPlayRadioByURL:streamedURL];
}

- (void)isPlayingNow {
    BOOL isPlayingNow = [[MediaPlayerManager sharedInstance] isPlayingNow];
    [self.interactorOut isPlaying:isPlayingNow];
}

- (void)stopStreamRadio {
    [self stopPlayRadio];
}

- (void)storeAudioStreamType:(RadioStationStreamType)audioType {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:audioType forKey:kStoredAudioStreamType];
    [defaults synchronize];
}
- (void)storeAudioQualityType:(RadioStationQualityType)qualityType {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:qualityType forKey:kStoredAudioQualityType];
    [defaults synchronize];
}

- (NSString *)iTunesURL {
    return self.song.trackViewURL;
}

- (RadioStation*)requestCurrentRadioStation {
    return self.currentRadioStation;
}

#pragma mark - MediaPlayerManagerDelegate

- (void)mediaPlayerStreamBecameAvailable {
    [self.interactorOut playerBeganStream];
}

- (void)mediaPlayerCanNotPlayRadioWithError:(NSError *)error {
    [self.interactorOut playerCanNotPlayStreamWithError:error];
}

- (void)streamInfoLoadedWithSong:(Song *)song {
    //TODO: A.S. Need to test. Probably need to create delegate from MediaPlayerManager
    //for method likelyToKeepUp
//    [self.interactorOut playerBeganStream];

    self.song = song;

    Song *savedSong = [SongLocalStorage retriveSongByTitle:song.title andArtistName:song.artistName];
    if (!savedSong) {
        [self.interactorOut becomeAvailableSong:self.song];
        [self.iTunesSearchManager searchMusicInfoBySong:song withDelegate:self];
        return;
    }
    
    self.song = savedSong;
    [self.interactorOut becomeAvailableSong:savedSong];
    [self.imageManager downloadImageByURL:[NSURL URLWithString:savedSong.albumArtwork300] delegate:self];
}

- (void)streamInfoNotLoadedWithError:(NSError*)error {
    
}

#pragma mark - iTunesSearchManagerDelegate

- (void)iTunesSearchManagerSongItemFound:(iTunesSearchItem *)foundedSongItem {
    [self.song addInfoFromITunesSearch:foundedSongItem];
    [SongLocalStorage saveSong:self.song];
    [self.interactorOut becomeAvailableSong:self.song];
    
    [self.imageManager downloadImageByURL:[NSURL URLWithString:foundedSongItem.artworkUrl300] delegate:self];
}

-(void)iTunesSearchManagerSongItemNotFoundWithError:(NSError *)error {
    
}

#pragma mark - ImageManagerDelegate

- (void)imageManagerDownloadedImage:(UIImage*)image {
    [self.interactorOut becomeAvailableAlbumImage:image];
}
- (void)imageManagerDidNotDonwloadImageWithError:(NSError*)error {
    
}

@end

//
//  PlayRadioViewController.m
//  RadioRecordApp
//
//  Created by Admin on 23.12.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

#import "RadioPlayerViewController.h"

#import "ASScrollableLabel.h"
#import "RadioPlayerControlPanelView.h"
#import "NavigationBarView.h"
#import "AlertView.h"
#import "ScrollableImage.h"

#import "UIImage+Utils.h"

#import "Song.h"

#import "AnimationConstants.h"
#import "NotificationKeys.h"

@interface RadioPlayerViewController () <NavigationBarViewDelegate, RadioPlayerControlPanelViewDelegate>
@property (weak, nonatomic) IBOutlet MPVolumeView *mpVolumeView;

@property (weak, nonatomic) IBOutlet NavigationBarView *navigationBarView;
@property (weak, nonatomic) IBOutlet ASScrollableLabel *songTitleLabel;
@property (weak, nonatomic) IBOutlet ASScrollableLabel *artistTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iTunesImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iTunesImageHeightConstraint;
@property (weak, nonatomic) IBOutlet ScrollableImage *scrollableImageView;

@property (weak, nonatomic) IBOutlet RadioPlayerControlPanelView *controlPanel;

@property (weak, nonatomic) IBOutlet UIImageView *volumeChangedImage;

@property (assign, nonatomic) CGFloat initialiTunesImageHeight;
@property (assign, nonatomic) BOOL albumConainterSet;
@property (assign, nonatomic) BOOL albumImageRotated;

@property (assign, nonatomic) BOOL click;

@end

@implementation RadioPlayerViewController

# pragma mark - Life cycle

+ (RadioPlayerViewController*)instanceFromStoryboard {
    
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RadioPlayerViewController* radioPlayerScreen =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"radioPlayerScreenIdentifier"];
    
    return radioPlayerScreen;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self.presenter requestRadioPlayerScreenConfiguration];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(applicationWillEnterFromBackground:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(volumeChanged:)
                                                 name:@"AVSystemController_SystemVolumeDidChangeNotification"
                                               object:nil];
    
    [self subcribeOnUIRemoteControlEvents];

    self.click = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.presenter radioPlayerScreenLoadedToPlayCurrentRadioStationWithAudioQuality:self.controlPanel.audioQuality];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.presenter radioPlayerScreenDismissed];
}

- (void)dealloc {
    RRLog(@"RadioPlayerViewController dismissed");
    
    //    [self unsubcribeFromUIRemoteControlEvents];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private

- (void)setupUI {
    self.navigationBarView.leftBarItem = NavigationBarItemBack;
    //TODO: A.S. Enable for debuging
    //self.navigationBarView.rightBarItem = NavigationBarItemBack;
    self.navigationBarView.delegate = self;
    
    self.controlPanel.delegate = self;
    
    [self setupVolumeView];
    [self setupAlbumContainer];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)setupVolumeView {

    if (SYSTEM_VERSION_LESS_THAN(@"9.0")) {
        [[UISlider appearanceWhenContainedIn:[MPVolumeView class], nil] setMinimumTrackImage:[[UIImage imageNamed:@"slider_max_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
        [[UISlider appearanceWhenContainedIn:[MPVolumeView class], nil] setMaximumTrackImage:[[UIImage imageNamed:@"slider_min_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
        [[UISlider appearanceWhenContainedIn:[MPVolumeView class], nil] setThumbImage:[[UIImage imageNamed:@"slider_polzunok_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
    } else {
        [[UISlider appearanceWhenContainedInInstancesOfClasses:@[[MPVolumeView class]]] setMinimumTrackImage:[[UIImage imageNamed:@"slider_max_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
        [[UISlider appearanceWhenContainedInInstancesOfClasses:@[[MPVolumeView class]]] setMinimumTrackImage:[[UIImage imageNamed:@"slider_min_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
        [[UISlider appearanceWhenContainedInInstancesOfClasses:@[[MPVolumeView class]]] setMinimumTrackImage:[[UIImage imageNamed:@"slider_polzunok_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 25)] forState:UIControlStateNormal];
    }
}

- (void)setupAlbumContainer {
    self.initialiTunesImageHeight = self.iTunesImageHeightConstraint.constant;
    self.iTunesImageHeightConstraint.constant = 0;
    self.albumConainterSet = NO;
    self.albumImageRotated = NO;
    
    [self.scrollableImageView setImage:[UIImage imageNamed:@"nota-icon"]];

    UITapGestureRecognizer* tapGestureRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(iTunesImageTapped)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    self.iTunesImageView.userInteractionEnabled = YES;
    [self.iTunesImageView addGestureRecognizer:tapGestureRecognizer];
}

- (void)subcribeOnUIRemoteControlEvents {
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)unsubcribeFromUIRemoteControlEvents {
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)setupVolumeImage:(CGFloat)volumeLevel {
    volumeLevel *=100;
    if (volumeLevel < 33) {
        self.volumeChangedImage.image = [UIImage imageNamed:@"volume-minimum-icon"];
    } else if (volumeLevel > 33 && volumeLevel < 66) {
        self.volumeChangedImage.image = [UIImage imageNamed:@"volume-middle-icon"];
    } else {
        self.volumeChangedImage.image = [UIImage imageNamed:@"volume-max-icon"];
    }
}

#pragma mark - Selectors

- (void)iTunesImageTapped {
    [self.presenter itunesImageTapped];
}

#pragma mark - Notifications

- (void)applicationWillEnterFromBackground:(NSNotification*)notification {
    RRLog(@"applicationWillEnterFromBackground");
}

- (void) volumeChanged:(NSNotification *)notify {
    CGFloat currentVolume = [notify.userInfo[@"AVSystemController_AudioVolumeNotificationParameter"] floatValue];
    [self setupVolumeImage:currentVolume];
}

#pragma mark - NavigationBarViewDelegate

- (void)navigationBarView:(NavigationBarView *)navigationBarView wasClickedLeftItem:(UIButton *)leftButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navigationBarView:(NavigationBarView *)navigationBarView wasClickedRightItem:(UIButton *)rightButton {
    //TODO: A.S. Enable for debuging
    self.click = !self.click;
    [self showAlbumImage: self.click ? [UIImage imageNamed:@"lock_screen_bg"] : nil];
}

#pragma mark - RadioPlayerControlPanelView

- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView *)controlPanelView audioQualityChangedState:(ControlPanelAudioQuality)state {
    [self.presenter updatedAudioQuality:state];
}

- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView *)controlPanelView mediaControlChangedIcon:(MediaControlIcon)icon {

    if (icon == MediaControlIconLoading) {
        [self.presenter startPlayingCurrentRadioStationWithAudioQuality:self.controlPanel.audioQuality];
    } else if (icon == MediaControlIconPlay) {
        [self.presenter stopPlayingRadio];
    }
}

- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView *)controlPanelView recordButtonChangedState:(ControlPanelRecordButtonState)state {
    
    AlertView* alertView = [[AlertView alloc] initWithTitle:Localized(@"IDS_ALERT_VIEW_ERROR_TITLE")
                                                    message:@"This functional isn't available yet"
                                                   delegate:nil
                                                    buttons:@[Localized(@"IDS_ALERT_VIEW_BUTTON_OK")]];
    [alertView showAboveView:self.view];
}

- (void)top100TappedOnRadioPlayerControlPanelView:(RadioPlayerControlPanelView *)controlPanelView {
    [self.presenter top100SongsTapped];
}

- (void)historyTappedOnRadioPlayerControlPanelView:(RadioPlayerControlPanelView *)controlPanelView {
    [self.presenter historyTapped];
}

#pragma mark - RadioPlayerView

- (void)showInfoForSong:(Song *)song {
    dispatch_async(dispatch_get_main_queue(), ^{

        self.artistTitleLabel.text = song.artistName.length ? song.artistName : Localized(@"IDS_RADIO_PLAY_SCREEN_WAITING_ARTIST");
        self.songTitleLabel.text = song.title.length ? song.title : Localized(@"IDS_RADIO_PLAY_SCREEN_WAITING_SONG");

        if (song.trackViewURL) {
            if (!self.albumConainterSet) {
                [self.view layoutIfNeeded];
                self.iTunesImageHeightConstraint.constant = self.initialiTunesImageHeight;
                self.albumConainterSet = YES;
            }

            [UIView animateWithDuration:kStandartAnimationTime
                             animations:^{
                                 [self.view layoutIfNeeded];
                             }
                             completion:nil];
        } else {

            if (self.albumConainterSet) {

                self.scrollableImageView.backgroundColor = [UIColor whiteColor];
                [self.scrollableImageView setImage:[UIImage imageNamed:@"nota-icon"]];
                [self.scrollableImageView updateSize];

                self.albumImageRotated = NO;
                self.albumConainterSet = NO;

                [UIView transitionWithView:self.scrollableImageView
                                  duration:3*kStandartAnimationTime
                                   options:UIViewAnimationOptionTransitionFlipFromRight
                                animations:nil
                                completion:^(BOOL finished) {

                                    [self.view layoutIfNeeded];
                                    self.iTunesImageHeightConstraint.constant = 0;
                                    [UIView animateWithDuration:kStandartAnimationTime
                                                     animations:^{
                                                         [self.view layoutIfNeeded];
                                                     }
                                                     completion:nil];
                                }];
            }
        }
        
    });

    
    //Uncomment when ui will be updated
    /*
     if (song.releaseDate) {
     NSDateFormatter* dateForamtter = [NSDateFormatter new];
     dateForamtter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
     NSDate* date = [dateForamtter dateFromString:song.releaseDate];
     NSCalendar* calendar = [NSCalendar currentCalendar];
     NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:date];
     
     NSString* year = @(components.year).stringValue;
     
     }*/
}

- (void)showPause {
    self.controlPanel.mediaControlIcon = MediaControlIconPause;
}

- (void)showStart {
    self.controlPanel.mediaControlIcon = MediaControlIconPlay;
}

- (void)showLoading {
    self.controlPanel.mediaControlIcon = MediaControlIconLoading;
}

- (void)showAlbumImage:(UIImage *)image {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (image) {
            if (!self.albumImageRotated) {
                UIImage* scaledImage = [UIImage imageWithImage:image
                                                  scaledToSize:CGSizeMake(CGRectGetWidth(self.view.frame),
                                                                          CGRectGetWidth(self.view.frame))];
                self.scrollableImageView.backgroundColor = [UIColor blackColor];
                [self.scrollableImageView setImage:scaledImage];
                [self.scrollableImageView updateSize];

                self.albumImageRotated = YES;

                [UIView transitionWithView:self.scrollableImageView
                                  duration:3*kStandartAnimationTime
                                   options:UIViewAnimationOptionTransitionFlipFromLeft
                                animations:nil
                                completion:^(BOOL finished) {
                                    [self.scrollableImageView scrollImageIfNeeded];

                                }];
            }
        }
    });
}

- (void)prepareRadioPlayerScreenWithAudioQuality:(ControlPanelAudioQuality)audioQuality {
    self.controlPanel.audioQuality = audioQuality;
}

- (void)prepareRadioPlayerScreenWithVolume:(CGFloat)volume {
    [self setupVolumeImage:volume];
}

- (void)showError:(NSError *)error {
    AlertView* alertView = [[AlertView alloc] initWithTitle:Localized(@"IDS_ALERT_VIEW_ERROR_TITLE")
                                                    message:error.localizedDescription
                                                   delegate:nil
                                                    buttons:@[Localized(@"IDS_ALERT_VIEW_BUTTON_OK")]];
    [alertView showAboveView:self.view];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if (self.controlPanel.mediaControlIcon == MediaControlIconPause) {
                [self.presenter stopPlayingRadio];
            } else {
                [self.presenter startPlayingCurrentRadioStationWithAudioQuality:self.controlPanel.audioQuality];
            }
            break;
        default:
            break;
    }
}

@end

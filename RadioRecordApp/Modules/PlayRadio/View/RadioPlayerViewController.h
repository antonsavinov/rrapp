//
//  RadioPlayerViewController.h
//  RadioRecordApp
//
//  Created by Admin on 23.12.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerScreenContract.h"

@interface RadioPlayerViewController : UIViewController <RadioPlayerView>

@property (strong, nonatomic) id<RadioPlayerPresenter>presenter;

+ (RadioPlayerViewController*)instanceFromStoryboard;

@end

//
//  RadioPlayerScreenPresenter.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerScreenContract.h"

@interface RadioPlayerPresenter : NSObject <RadioPlayerPresenter>

@property (strong, nonatomic) id<RadioPlayerInteractorIn>interactorIn;
@property (weak, nonatomic)   id<RadioPlayerView>view;
@property (strong, nonatomic) id<RadioPlayerRouter>router;

@end

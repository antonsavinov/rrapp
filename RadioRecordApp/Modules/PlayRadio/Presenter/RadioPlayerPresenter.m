//
//  RadioPlayerScreenPresenter.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerPresenter.h"

@interface RadioPlayerPresenter () <RadioPlayerInteractorOut>
@end

@implementation RadioPlayerPresenter

#pragma mark - Life cycle

- (void)dealloc {
    RRLog(@"RadioPlayerPresenter dismissed");
}

#pragma mark - RadioPlayerPresenter

- (void)requestRadioPlayerScreenConfiguration {
    [self.interactorIn checkStoredAudioStreamTypeAndQuality];
    [self.interactorIn checkVolume];
}

- (void)radioPlayerScreenLoadedToPlayCurrentRadioStationWithAudioQuality:(ControlPanelAudioQuality)audioQuality {
    
    RadioStationStreamType streamType;
    RadioStationQualityType qualityType;

    [self mapAudioQuality:audioQuality onStreamType:&streamType andQualityType:&qualityType];
    [self.interactorIn startStreamRadioImmediatelyifCanWithAudioStreamType:streamType
                                                                andQuality:qualityType];
}

- (void)radioPlayerScreenDismissed {

}

- (void)startPlayingCurrentRadioStationWithAudioQuality:(ControlPanelAudioQuality)audioQuality {

    RadioStationStreamType streamType;
    RadioStationQualityType qualityType;

    [self mapAudioQuality:audioQuality onStreamType:&streamType andQualityType:&qualityType];
    [self.interactorIn startStreamRadioWithAudioStreamType:streamType
                                                andQuality:qualityType];
}

- (void)stopPlayingRadio {
    [self.interactorIn stopStreamRadio];
}

- (void)updatedAudioQuality:(ControlPanelAudioQuality)audioQuality {
    RadioStationStreamType streamType;
    RadioStationQualityType qualityType;

    [self mapAudioQuality:audioQuality onStreamType:&streamType andQualityType:&qualityType];
    [self.interactorIn storeAudioStreamType:streamType];
    [self.interactorIn storeAudioQualityType:qualityType];
    [self.interactorIn startStreamRadioImmediatelyifCanWithAudioStreamType:streamType
                                                                andQuality:qualityType];
}

- (void)auaioTypeWasChangedOnState:(RadioStationStreamType)audioType {
    [self.interactorIn storeAudioStreamType:audioType];
}

- (void)audioQualityWasChangedOnState:(RadioStationQualityType)qualityType {
    [self.interactorIn storeAudioQualityType:qualityType];
}

- (void)itunesImageTapped {
    NSString* iTunesURL = [self.interactorIn iTunesURL];
    [self.router launchiTunesAppByURL:iTunesURL];
}

- (void)top100SongsTapped {
    RadioStation* radioStation = [self.interactorIn requestCurrentRadioStation];
    [self.router presentTop100SongsScreenForCurrentStation:radioStation];
}

- (void)historyTapped {
    RadioStation* radioStation = [self.interactorIn requestCurrentRadioStation];
    [self.router presentHistoryScreenForCurrentStation:radioStation];
}

#pragma mark - RadioPlayerInteractorOut

- (void)playerWillStream {
    [self.view showLoading];
}

- (void)playerBeganStream {
    [self.view showPause];
}

- (void)playerStoppedStream {
    [self.view showStart];
}

- (void)becomeAvailableSong:(Song *)song {
    [self.view showInfoForSong:song];
}

- (void)isPlaying:(BOOL)isPlaying {
    if (isPlaying) {
        [self.view showPause];
    } else {
        [self.view showStart];
    }
}

- (void)becomeAvailableAlbumImage:(UIImage *)image {
    [self.view showAlbumImage:image];
}

- (void)storedAudioStream:(RadioStationStreamType)audioStream
               andQuality:(RadioStationQualityType)quality {
    ControlPanelAudioQuality audioQuality;
    
    if (audioStream == RadioStationStreamTypeAAC && quality == RadioStationQualityTypeLow) {
        audioQuality = ControlPanelAudioQualityLow;
    } else if (audioStream == RadioStationStreamTypeAAC && quality == RadioStationQualityTypeHigh) {
        audioQuality = ControlPanelAudioQualityMedium;
    } else {
        audioQuality = ControlPanelAudioQualityHigh;
    }
    
    [self.view prepareRadioPlayerScreenWithAudioQuality:audioQuality];
}

- (void)playerCanNotPlayStreamWithError:(NSError *)error {
    [self.view showError:error];
    [self.view showStart];
}

- (void)currentVolume:(CGFloat)volume {
    [self.view prepareRadioPlayerScreenWithVolume:volume];
}

#pragma mark - Private

- (void)mapAudioQuality:(ControlPanelAudioQuality)audioQuality
           onStreamType:(RadioStationStreamType *)streamType
         andQualityType:(RadioStationQualityType *)qualityType {
    switch (audioQuality) {
        case ControlPanelAudioQualityLow:
            *streamType = RadioStationStreamTypeAAC;
            *qualityType = RadioStationQualityTypeLow;
            break;
        case ControlPanelAudioQualityMedium:
            *streamType = RadioStationStreamTypeAAC;
            *qualityType = RadioStationQualityTypeHigh;
            break;

        case ControlPanelAudioQualityHigh:
            *streamType = RadioStationStreamTypeMP3;
            *qualityType = RadioStationQualityTypeHigh;
            break;
    }
}

@end

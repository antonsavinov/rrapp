//
//  UILabel+Utils.m
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "UILabel+Utils.h"

@implementation UILabel (Utils)

-(void)applyLineSpacing:(CGFloat)lineSpacing
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 0;
    paragraphStyle.lineHeightMultiple = lineSpacing;
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    paragraphStyle.alignment = self.textAlignment;
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    [attributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedText.length)];
    self.attributedText = attributedText;
}

@end

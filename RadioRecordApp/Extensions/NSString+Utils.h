//
//  NSString+Utils.h
//  RadioRecordApp
//
//  Created by Admin on 11.03.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (NSString*)convertSpacesIfNeeded;

@end

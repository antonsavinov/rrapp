//
//  NSData+Utils.m
//  RadioRecordApp
//
//  Created by Admin on 12.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "NSData+Utils.h"

@implementation NSData (Utils)

- (NSString*)mimeType {
    uint8_t c;
    [self getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

@end

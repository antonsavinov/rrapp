//
//  UIColor+Utils.m
//  RadioRecordApp
//
//  Created by Admin on 08.01.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *)radioRecordGrayColor {
    UIColor* radioRecordGrayColor = [UIColor colorWithRed:38.f/255 green:38.f/255 blue:38.f/255 alpha:1.f];
    return radioRecordGrayColor;
}

+ (UIColor *)radioRecordLightPinkColor {
    UIColor* radioRecordLightPinkColor = [UIColor colorWithRed:223.f/255 green:33.f/255 blue:133.f/255 alpha:1.f];
    return radioRecordLightPinkColor;
}

+ (UIColor *)radioRecordDarkPinColor {
    UIColor* radioRecordDarkPinColor = [UIColor colorWithRed:155.f/255 green:33.f/255 blue:103.f/255 alpha:1.f];
    return radioRecordDarkPinColor;
}

+ (UIColor *)radioRecordWhiteColor {
    UIColor* radioRecordWhiteColor = [UIColor colorWithRed:250.f/255 green:250.f/255 blue:250.f/255 alpha:1.f];
    return radioRecordWhiteColor;
}

@end

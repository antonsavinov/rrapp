//
//  UILabel+Utils.h
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utils)

-(void)applyLineSpacing:(CGFloat)lineSpacing;

@end

//
//  NSString+Translation.h
//  RadioRecordApp
//
//  Created by Admin on 11.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Translation)

- (NSString* )toLatin;

@end

//
//  NSData+Utils.h
//  RadioRecordApp
//
//  Created by Admin on 12.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Utils)

- (NSString*)mimeType;

@end

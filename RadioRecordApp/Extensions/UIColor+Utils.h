//
//  UIColor+Utils.h
//  RadioRecordApp
//
//  Created by Admin on 08.01.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+ (UIColor *)radioRecordGrayColor;
+ (UIColor *)radioRecordLightPinkColor;
+ (UIColor *)radioRecordDarkPinColor;
+ (UIColor *)radioRecordWhiteColor;

@end

//
//  UIImage+Utils.h
//  RadioRecordApp
//
//  Created by Admin on 18.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;

@end

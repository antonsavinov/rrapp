//
//  UIView+Autolayout.m
//  RadioRecordApp
//
//  Created by Admin on 29.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "UIView+Autolayout.h"

@implementation UIView (Autolayout)

- (void)matchToParent {
    [self matchToParentWithPriority:UILayoutPriorityRequired];
}

- (void)matchToParentWithPriority:(UILayoutPriority)priority {
    if (self.superview) {
        NSArray *hConstraints = [NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|-0-[self]-0-|"
                                 options:0
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(self)];
        for (NSLayoutConstraint *constraint in hConstraints) {
            constraint.priority = priority;
        }
        [self.superview addConstraints:hConstraints];
        NSArray *vConstraints = [NSLayoutConstraint
                                 constraintsWithVisualFormat:@"V:|-0-[self]-0-|"
                                 options:0
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(self)];
        for (NSLayoutConstraint *constraint in vConstraints) {
            constraint.priority = priority;
        }
        [self.superview addConstraints:vConstraints];
    }
}

@end

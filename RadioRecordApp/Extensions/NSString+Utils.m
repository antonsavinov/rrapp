//
//  NSString+Utils.m
//  RadioRecordApp
//
//  Created by Admin on 11.03.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSString*)convertSpacesIfNeeded {
    NSRange whiteSpaceRange = [self rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *convertedString = nil;
    if (whiteSpaceRange.location != NSNotFound) {
        convertedString = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return (convertedString) ? convertedString : self;
}

@end

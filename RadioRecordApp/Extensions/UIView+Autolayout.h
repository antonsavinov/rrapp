//
//  UIView+Autolayout.h
//  RadioRecordApp
//
//  Created by Admin on 29.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Autolayout)

- (void)matchToParent;
- (void)matchToParentWithPriority:(UILayoutPriority)priority;

@end

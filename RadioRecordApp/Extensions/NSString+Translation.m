//
//  NSString+Translation.m
//  RadioRecordApp
//
//  Created by Admin on 11.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "NSString+Translation.h"

@implementation NSString (Translation)

- (NSString* )toLatin {
    
    NSMutableString* translationString = [self mutableCopy];
    CFMutableStringRef bufferRef = (__bridge CFMutableStringRef)translationString;
    CFStringTransform(bufferRef, NULL, kCFStringTransformToLatin, false);
    CFStringTransform(bufferRef, NULL, kCFStringTransformStripCombiningMarks, false);
    
    return translationString;
}

@end

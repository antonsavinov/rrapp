//
//  AlertView.m
//  RadioRecordApp
//
//  Created by Admin on 17.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "AlertView.h"

#import "AnimationConstants.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

static const CGFloat maxAlertWidth = 300.;
static const CGFloat maxAlertHeight = 350.;

static const CGFloat startAlertHeight = 8.;

// Paddings
static const CGFloat horizontalPadding = 8.;
static const CGFloat horizontalSpace = 16.;
static const CGFloat leftPadding = 8.;

//Section of title
static const CGFloat titleHeight = 30.;

//Section of button
static const CGFloat buttonHeight = 40.;

//Section of message
static const CGFloat minMessageFont = 12.;

//Section of image
static const CGFloat maxImageWidth = 100;
static const CGFloat maxImageHeight = 100;


@interface AlertView ()

@property (nonatomic, weak) id<AlertViewDelegate>delegate;
@property (nonatomic, strong) UIView* alertView;

@end

@implementation AlertView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<AlertViewDelegate>)delegate buttons:(NSArray<NSString *> *)buttons {
    
    return [self initWithTitle:title message:message image:nil delegate:delegate buttons:buttons];
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage *)image delegate:(id<AlertViewDelegate>)delegate buttons:(NSArray<NSString *> *)buttons {
    
    CGRect backGroundFrame;
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIDeviceOrientationLandscapeLeft ||
        currentOrientation == UIDeviceOrientationLandscapeRight) {
        backGroundFrame = CGRectMake(0.0, 0.0, CGRectGetHeight([UIScreen mainScreen].bounds), CGRectGetWidth([UIScreen mainScreen].bounds));
    } else {
        backGroundFrame = CGRectMake(0.0, 0.0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
    }
    
    if (self = [super initWithFrame:backGroundFrame]) {
        self.delegate = delegate;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        
        UILabel* titleLabel;
        
        CGFloat currentAlertHeight = startAlertHeight;
        
        if (title) {
            
            titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding,
                                                                   currentAlertHeight,
                                                                   maxAlertWidth - 2*leftPadding,
                                                                   titleHeight)];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.font = [UIFont fontWithName:@"EuropeExt-Italic" size:17.0];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.minimumScaleFactor = 5.0;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor blackColor];
            titleLabel.text = title;
            
            currentAlertHeight += CGRectGetHeight(titleLabel.frame) + horizontalPadding;
            
        } else {
            currentAlertHeight += horizontalSpace;
        }
        
        UIImageView* imageView;
        if (image) {
            
            if(image.size.height > maxImageHeight || image.size.width > maxImageWidth ) {
                image = [UIImage imageWithImage:image
                                   scaledToSize:CGSizeMake(maxImageWidth, maxImageHeight)];
            }
            
            imageView = [[UIImageView alloc] initWithImage:image];
            imageView.frame = CGRectMake(maxAlertWidth/2-(CGRectGetWidth(imageView.frame)/2),
                                         currentAlertHeight,
                                         CGRectGetWidth(imageView.frame),
                                         CGRectGetHeight(imageView.frame));
            
            currentAlertHeight += CGRectGetHeight(imageView.frame) + horizontalPadding;
            
        }
        
        UILabel* messageLabel;
        UIScrollView* messageScrollView;
        
        if (message) {
            CGFloat maxMessageHeight = maxAlertHeight - currentAlertHeight - horizontalPadding;
            if (buttons) {
                maxMessageHeight -= buttonHeight * buttons.count;
            }
            
            messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding,
                                                                     0,
                                                                     maxAlertWidth - 4 * leftPadding,
                                                                     0)];
            messageLabel.numberOfLines = 0;
            messageLabel.font = [UIFont fontWithName:@"EuropeExtNormal" size:14.0];
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.backgroundColor = [UIColor clearColor];
            messageLabel.textColor = [UIColor blackColor];
            messageLabel.text = message;
            [messageLabel sizeToFit];
            messageLabel.frame = CGRectMake(leftPadding,
                                            0,
                                            maxAlertWidth - 4 * leftPadding,
                                            CGRectGetHeight(messageLabel.frame));
            
            while (CGRectGetHeight(messageLabel.frame) > maxMessageHeight &&
                   messageLabel.font.pointSize > minMessageFont) {
                messageLabel.font = [UIFont fontWithName:@"EuropeExtNormal"
                                                    size:messageLabel.font.pointSize - 1];
                [messageLabel sizeToFit];
                messageLabel.frame = CGRectMake(leftPadding,
                                                0,
                                                maxAlertWidth - 4 * leftPadding,
                                                CGRectGetHeight(messageLabel.frame));
            }
            
            CGFloat scrollViewHeight = (CGRectGetHeight(messageLabel.frame) > maxMessageHeight) ? maxMessageHeight:CGRectGetHeight(messageLabel.frame);
            
            messageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(leftPadding,
                                                                               currentAlertHeight,
                                                                               maxAlertWidth - 2*leftPadding,
                                                                               scrollViewHeight)];
            messageScrollView.contentSize = messageLabel.frame.size;
            [messageScrollView addSubview:messageLabel];
            
            currentAlertHeight += CGRectGetHeight(messageScrollView.frame) + horizontalSpace;
        }
        else {
            currentAlertHeight += horizontalSpace;
        }
        
        NSMutableArray<UIButton*>* uiButtons = nil;
        if (buttons) {
            uiButtons = [NSMutableArray new];
            CGFloat initialY = currentAlertHeight;
            for (NSUInteger index = 0; index < buttons.count; index++) {
                
                UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0,
                                                                              initialY,
                                                                              maxAlertWidth,
                                                                              buttonHeight)];
                button.tag = index;
                [button setTitle:buttons[index] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont fontWithName:@"EuropeExt-Bold-Italic"
                                                         size:16.0];
                button.titleLabel.textColor = [UIColor whiteColor];
                button.backgroundColor = [UIColor radioRecordLightPinkColor];
                [button addTarget:self
                           action:@selector(onButtonPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
                
                initialY += buttonHeight;
                
                currentAlertHeight += buttonHeight;
                
                if (index < buttons.count - 1) {
                    initialY += 1;
                    currentAlertHeight += 1;
                }
                
                [uiButtons addObject:button];
            }
        }
        
        CGRect alertViewFrame = CGRectMake((CGRectGetWidth(self.frame)-maxAlertWidth)/2.,
                                           (CGRectGetHeight(self.frame)-currentAlertHeight)/2,
                                           maxAlertWidth,
                                           currentAlertHeight);
        _alertView = [[UIView alloc] initWithFrame:alertViewFrame];
        _alertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.cornerRadius = 5.f;
        _alertView.layer.masksToBounds = YES;
        
        [self addSubview:_alertView];
        
        if (titleLabel){
            [_alertView addSubview:titleLabel];
        }
        
        if (imageView) {
            [_alertView addSubview:imageView];
        }
        
        if (messageScrollView) {
            [_alertView addSubview:messageScrollView];
        }
        
        for (UIButton* button in uiButtons) {
            [_alertView addSubview:button];
        }
    }
    
    return self;
}

#pragma mark - Public Methods

- (void)showAboveView:(UIView *)view {
    if ([view isKindOfClass:[UIView class]])
    {
        [view addSubview:self];
        [self animateShow];
    }
}

#pragma mark - Private methods

- (void)animateShow {
    self.alpha = 0;
    self.hidden = NO;
    [UIView animateWithDuration:kStandartAnimationTime
                     animations:^{
                         self.alpha = 1;
                     }];
}

- (void)animateHide {
    
    [UIView animateWithDuration:kStandartAnimationTime
                     animations:^{
                         self.alpha = 0;
                     }
                     completion: ^(BOOL finished) {
                         self.hidden = YES;
                         [self removeFromSuperview];
                     }];
}

#pragma mark - Selectors

- (void)onButtonPressed:(UIButton*)sender {
    
    if ([self.delegate respondsToSelector:@selector(alertView:wasTappedByButtonWithIndex:)]) {
        [self.delegate alertView:self wasTappedByButtonWithIndex:sender.tag];
    }
    [self animateHide];
}

@end

//
//  CustomSwitchButton.h
//  TouchTunesv3
//
//  Created by Alexey Vasilyev on 26.11.15.
//  Copyright © 2015 TouchTunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomSwitchButtonDelegate;

@interface CustomSwitchButton : UIView

@property (nonatomic, assign, getter=isSelected) BOOL selected;

@property (nonatomic, weak) id<CustomSwitchButtonDelegate> delegate;
@property (nonatomic, strong) NSString* selectedStateText;
@property (nonatomic, strong) NSString* deselectedStateText;
@property (nonatomic, strong) UIFont* textFont;

@end

@protocol CustomSwitchButtonDelegate <NSObject>

- (void)switchButton:(CustomSwitchButton*)switchButton wasChangedState:(BOOL)isOn;

@end

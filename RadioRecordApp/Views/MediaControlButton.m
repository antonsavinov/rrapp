//
//  PlayButton.m
//  PlayButtonProject
//
//  Created by Anton Savinov on 13/03/2018.
//  Copyright © 2018 Harman. All rights reserved.
//

#import "MediaControlButton.h"
#import "ActivityIndicator.h"

@interface MediaControlButton ()
@property (nonatomic, strong) UIImage *animatedImage;
@property (nonatomic, strong) ActivityIndicator *activityIndicator;
@end

@implementation MediaControlButton

- (instancetype)init {
    if (self = [super init]) {
        [self generalInitialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self generalInitialization];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self generalInitialization];
    }
    return self;
}

- (void)generalInitialization {
    [self setAdjustsImageWhenHighlighted:NO];
    self.activityIndicator = [ActivityIndicator new];
}

- (void)setIcon:(MediaControlIcon)icon {
    _icon = icon;

    if (_icon == MediaControlIconPlay) {

        [self setImage:[UIImage imageNamed:@"play-icon"] forState:UIControlStateNormal];

    } else if (_icon == MediaControlIconLoading) {

        if (![self.imageView isAnimating]) {
            if (!self.imageView.animationImages) {
                self.imageView.animationImages = self.activityIndicator.animationImages;
                self.imageView.animationDuration = self.activityIndicator.animationDuration;
            }
            [self.imageView startAnimating];
        }
    } else {
        if ([self.imageView isAnimating]) {
            [self.imageView stopAnimating];
        }

        [self setImage:[UIImage imageNamed:@"pause-icon"] forState:UIControlStateNormal];
    }
}

@end

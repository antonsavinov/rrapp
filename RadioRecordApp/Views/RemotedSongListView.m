//
//  RemotedSongListView.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 14/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "RemotedSongListView.h"
#import "UIView+Autolayout.h"

#import "HistorySongCell.h"
#import "TopSongCell.h"

#import "HistorySong.h"
#import "TopSong.h"

#import "ActivityIndicator.h"

@interface RemotedSongListView () <HistorySongCellDelegate, TopSongCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet ActivityIndicator *loadingIndicatorImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RemotedSongListView

#pragma mark - Life cycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self load];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self load];
    }
    return self;
}

#pragma mark - Public

- (void)updateSongAtIndex:(NSUInteger)index {
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)showLoadingScreen {
    self.loadingView.hidden = NO;
    self.tableView.hidden = YES;
    self.loadingIndicatorImageView.hidden = NO;
    [self.loadingIndicatorImageView startAnimating];
    self.messageLabel.text = @"Fetching data...";
}

- (void)refreshSongs {
    [self.loadingIndicatorImageView stopAnimating];
    self.loadingView.hidden = YES;
    self.tableView.hidden = NO;
    [self.tableView reloadData];
}

- (void)showEmptyView {
    [self.loadingIndicatorImageView stopAnimating];
    self.loadingIndicatorImageView.hidden = YES;
    self.loadingView.hidden = NO;
    self.tableView.hidden = YES;
    self.messageLabel.text = @"Content isn't available";
}

- (void)setMessage:(NSString *)message {
    self.messageLabel.text = message;
}

- (void)updateSongDurationAtIndex:(NSUInteger)index {
    id <RemotedSongProtocol, HistorySongProtocol> song = [self.delegate songAtIndex:index];
    if ([song conformsToProtocol:@protocol(RemotedSongProtocol)] &&
        [song conformsToProtocol:@protocol(HistorySongProtocol)]) {
        id<RemotedSongCellProtocol> cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        [cell setDuration:[song duration]];
    }
}

- (void)songFinishedPlayingAtIndex:(NSUInteger)index {
    id cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    if ([cell conformsToProtocol:@protocol(RemotedSongCellProtocol)]) {
        [cell setPlayState:NO];
    }
}

#pragma mark - Private

- (void)load {
    
    UIView *xibView =
    [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"RemotedSongListView" owner:self options:nil] firstObject];
    [self addSubview:xibView];
    xibView.translatesAutoresizingMaskIntoConstraints = NO;
    [xibView matchToParent];
}

- (void)loadTableViewCells {
    if (self.listType == RemotedSongListTypeHistory) {
        UINib *nib = [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"HistorySongCell" owner:self options:nil] objectAtIndex:0];
        [self.tableView registerNib:nib
             forCellReuseIdentifier:[HistorySongCell identifier]];
    } else if (self.listType == RemotedSongListTypeTop100) {

    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.delegate numberOfSongs];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    //TODO: A.S. Think about logic here!!!

    id <RemotedSongProtocol, HistorySongProtocol> song = [self.delegate songAtIndex:indexPath.row];

    if ([song conformsToProtocol:@protocol(RemotedSongProtocol)] &&
        [song conformsToProtocol:@protocol(HistorySongProtocol)]) {

        HistorySongCell* cell = [tableView dequeueReusableCellWithIdentifier:[HistorySongCell identifier]];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistorySongCell" owner:self options:nil] objectAtIndex:0];
        }

        [cell setSongTitle:song.songTitle];
        [cell setArtistName:song.artistName];
        [cell setStartTime:song.startTime];
        [cell setDuration:song.duration];
        [cell setPlayState:[self.delegate isSongPlayedAtIndex:indexPath.row]];
        cell.delegate = self;

        return cell;

    } else if ([song conformsToProtocol:@protocol(RemotedSongProtocol)]) {

        TopSongCell* cell = [tableView dequeueReusableCellWithIdentifier:[TopSongCell identifier]];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"TopSongCell" owner:self options:nil] objectAtIndex:0];
        }

        [cell setArtistName:song.artistName];
        [cell setSongTitle:song.songTitle];
        [cell setPlayState:[self.delegate isSongPlayedAtIndex:indexPath.row]];
        cell.delegate = self;

        return cell;
    } else {
        return nil;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.listType == RemotedSongListTypeTop100 ? 60.f : 70.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - HistorySongCellDelegate

- (void)historySongCell:(HistorySongCell *)cell playButtonChangedState:(BOOL)isPlaying {
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    if (isPlaying) {
        [self.delegate songTappedForPlayingByIndex:indexPath.row];
    } else {
        [self.delegate songTappedForStopping];
    }
}

#pragma mark - TopSongCellDelegate

- (void)topSongCell:(TopSongCell *)cell playButtonChangedState:(BOOL)isPlaying {
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    if (isPlaying) {
        [self.delegate songTappedForPlayingByIndex:indexPath.row];
    } else {
        [self.delegate songTappedForStopping];
    }
}

@end

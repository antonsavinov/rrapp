//
//  RemotedSongCellProtocol.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 15/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RemotedSongCellProtocol <NSObject>
- (void)setArtistName:(NSString*)artistName;
- (void)setSongTitle:(NSString*)songTitle;
- (void)setDuration:(CGFloat)duration;
- (void)setPlayState:(BOOL)isPlaying;
@end

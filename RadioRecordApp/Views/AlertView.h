//
//  AlertView.h
//  RadioRecordApp
//
//  Created by Admin on 17.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertViewDelegate;

@interface AlertView : UIView

- (instancetype)initWithTitle:(NSString*)title
                      message:(NSString*)message
                        image:(UIImage*)image
                     delegate:(id<AlertViewDelegate>)delegate
                      buttons:(NSArray<NSString*>*)buttons;

- (instancetype)initWithTitle:(NSString*)title
                      message:(NSString*)message
                     delegate:(id<AlertViewDelegate>)delegate
                      buttons:(NSArray<NSString*>*)buttons;

- (void)showAboveView:(UIView*)view;

@end

@protocol AlertViewDelegate <NSObject>

@optional
- (void)alertView:(AlertView*)alertView wasTappedByButtonWithIndex:(NSUInteger)index;

@end

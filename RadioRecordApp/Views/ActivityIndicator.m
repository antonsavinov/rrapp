//
//  ActivityIndicator.m
//  RadioRecordApp
//
//  Created by Anton Savinov on 16/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "ActivityIndicator.h"

@implementation ActivityIndicator

- (instancetype)init {
    if (self = [super init]) {
        [self generalInitialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self generalInitialization];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self generalInitialization];
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)image {
    if (self = [super initWithImage:image]) {
        [self generalInitialization];
    }
    return self;
}

- (void)generalInitialization {

    NSArray<NSString *> *imagePaths = [[NSBundle mainBundle] pathsForResourcesOfType:@"png"
                                                                         inDirectory:@"ActivityIndicator"];

    NSMutableArray <NSString *> *filteredPathsWithoutScale = [NSMutableArray new];

    for (NSString *path in imagePaths) {
        if (!([path hasSuffix:@"2x.png"] || [path hasSuffix:@"3x.png"])) {
            [filteredPathsWithoutScale addObject:path];
        }
    }

    NSMutableArray<UIImage *> *animationImages = [NSMutableArray new];
    for (NSString *path in filteredPathsWithoutScale) {
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        [animationImages addObject:image];
    }

    self.animationImages = animationImages;
    self.animationDuration = 0.6;
}

@end

//
//  RadioPlayerControlPanelView.h
//  RadioRecordApp
//
//  Created by Admin on 04.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MediaControlButton.h"

typedef NS_ENUM(NSUInteger, ControlPanelRecordButtonState) {
    ControlPanelRecordButtonStateRecording = 0,
    ControlPanelRecordButtonStateNotRecording
};

typedef NS_ENUM(NSUInteger, ControlPanelAudioQuality) {
    ControlPanelAudioQualityLow = 0,
    ControlPanelAudioQualityMedium,
    ControlPanelAudioQualityHigh
};

@protocol RadioPlayerControlPanelViewDelegate;

@interface RadioPlayerControlPanelView : UIView

@property (nonatomic, assign) MediaControlIcon mediaControlIcon;
@property (nonatomic, assign) ControlPanelAudioQuality audioQuality;
@property (nonatomic, assign) ControlPanelRecordButtonState recordButtonState;
@property (nonatomic, weak) id<RadioPlayerControlPanelViewDelegate>delegate;

@end

@protocol RadioPlayerControlPanelViewDelegate <NSObject>

- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView*)controlPanelView mediaControlChangedIcon:(MediaControlIcon)icon;
- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView*)controlPanelView recordButtonChangedState:(ControlPanelRecordButtonState)state;
- (void)radioPlayerControlPanelView:(RadioPlayerControlPanelView*)controlPanelView audioQualityChangedState:(ControlPanelAudioQuality)state;

- (void)top100TappedOnRadioPlayerControlPanelView:(RadioPlayerControlPanelView*)controlPanelView;
- (void)historyTappedOnRadioPlayerControlPanelView:(RadioPlayerControlPanelView*)controlPanelView;

@end

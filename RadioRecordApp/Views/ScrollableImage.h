//
//  ScrollableImage.h
//  RadioRecordApp
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollableImage : UIView

@property (nonatomic, assign) CGFloat                speedOfScrolling;
@property (nonatomic, assign) NSTimeInterval         timeOfPause;
@property (nonatomic, assign) UIViewAnimationOptions animationOptions;

- (void)setImage:(UIImage*)image;
- (void)setBackgroundColor:(UIColor *)backgroundColor;
- (void)scrollImageIfNeeded;
- (void)updateSize;

@end

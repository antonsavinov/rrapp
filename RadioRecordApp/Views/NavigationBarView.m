//
//  NavigationBarView.m
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "NavigationBarView.h"
#import "UIView+Autolayout.h"
#import "UILabel+Utils.h"

BOOL const isDebug = YES;

@interface NavigationBarView ()

@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIView *centralView;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@end

@implementation NavigationBarView

#pragma mark - Life cycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self load];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self load];
    }
    return self;
}

- (void)load
{
    UIView *xibView =
    [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"NavigationBarView" owner:self options:nil] firstObject];
    [self addSubview:xibView];
    xibView.translatesAutoresizingMaskIntoConstraints = NO;
    [xibView matchToParent];
    
    if (isDebug)
    {
        self.leftView.layer.borderColor = self.rightView.layer.borderColor = self.centralView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    self.leftView.backgroundColor = self.rightView.backgroundColor = self.centralView.backgroundColor = [UIColor colorWithRed:41 green:38 blue:57 alpha:0];
    
    [self setupLogo];
}

#pragma mark - Custom Accessors

- (void)setLeftBarItem:(NavigationBarItem)leftBarItem
{
    _leftBarItem = leftBarItem;
    
    [self convertNavigationBarItem:leftBarItem toImageOnButton:self.leftButton];
}

- (void)setRightBarItem:(NavigationBarItem)rightBarItem
{
    _rightBarItem = rightBarItem;
    
    [self convertNavigationBarItem:rightBarItem toImageOnButton:self.rightButton];
}

#pragma mark - Private

- (void)convertNavigationBarItem:(NavigationBarItem)item toImageOnButton:(UIButton*)button
{
    if (item == NavigationBarItemUnknown) return;
    
    NSString* imageName = nil;
    
    switch (item)
    {
        case NavigationBarItemBack:
            imageName = @"back-icon";
            break;
            
        default:
            break;
    }
    
    if (!imageName) return;
    
    UIImage* image = [UIImage imageNamed:imageName];
    
    if (!image) return;
    
    [button setImage:image forState:UIControlStateNormal];
}

- (void)setupLogo {
    
    NSString* partOneString = @"RECORD\n";
    NSString* partTwoString = @"DANCE RADIO";
    
    NSMutableAttributedString* attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",partOneString, partTwoString]];
    
    UIFont* font = [UIFont fontWithName:@"Record_beta" size:32];
    
    [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, partOneString.length)];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Record_beta" size:16] range:NSMakeRange(partOneString.length, partTwoString.length)];
    
    self.titleLabel.attributedText = attributedString;
    
    [self.titleLabel applyLineSpacing:0.75];
}

#pragma mark - IBActions

- (IBAction)leftItemWasClicked:(UIButton *)sender {
    
    if (_leftBarItem == NavigationBarItemUnknown) return;
    
    if ([self.delegate respondsToSelector:@selector(navigationBarView:wasClickedLeftItem:)]) {
        [self.delegate navigationBarView:self wasClickedLeftItem:sender];
    }
}
- (IBAction)rightItemWasClicked:(UIButton *)sender {
    
    if (_leftBarItem == NavigationBarItemUnknown) return;
    
    if ([self.delegate respondsToSelector:@selector(navigationBarView:wasClickedRightItem:)]) {
        [self.delegate navigationBarView:self wasClickedRightItem:sender];
    }
}

@end

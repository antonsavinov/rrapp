//
//  RemotedSongListView.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 14/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RemotedSongProtocol.h"
#import "HistorySongProtocol.h"

@protocol RemotedSongListViewDelegate;

typedef NS_ENUM(NSUInteger, RemotedSongListType) {
    RemotedSongListTypeTop100 = 0,
    RemotedSongListTypeHistory
};

@interface RemotedSongListView : UIView

@property (nonatomic, weak) id<RemotedSongListViewDelegate>delegate;
@property (nonatomic, assign) RemotedSongListType listType;

- (void)showLoadingScreen;
- (void)refreshSongs;
- (void)showEmptyView;

- (void)updateSongDurationAtIndex:(NSUInteger)index;
- (void)songFinishedPlayingAtIndex:(NSUInteger)index;

- (void)setMessage:(NSString *)message;

@end

@protocol RemotedSongListViewDelegate <NSObject>

- (NSUInteger)numberOfSongs;
- (id<RemotedSongProtocol, HistorySongProtocol>)songAtIndex:(NSUInteger)index;
- (BOOL)isSongPlayedAtIndex:(NSUInteger)index;
- (void)songTappedForPlayingByIndex:(NSUInteger)index;
- (void)songTappedForStopping;

@end

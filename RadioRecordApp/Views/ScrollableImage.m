//
//  ScrollableImage.m
//  RadioRecordApp
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "ScrollableImage.h"

#define kDefaultPixelsPerSecond 30
#define kDefaultPauseTime 1.f

@interface ScrollableImage ()

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIImageView*  imageView;
@property (nonatomic, assign) BOOL          isTop;

@end

@implementation ScrollableImage

#pragma mark - Life cycle

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self generalInit];
        [self subscribeNotifications];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self generalInit];
        [self subscribeNotifications];
    }
    
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public

- (void)setImage:(UIImage*)image {
    
    if (!image || _imageView.image == image) return;
    
    _imageView.image = image;
    //    TODO: A.S. Commented to avoid automaticaly scrolling image
    //    [self refreshImageView];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    _scrollView.backgroundColor = backgroundColor;
}

- (void)scrollImageIfNeeded {
    
    if (!self.imageView.image) return;
    
    CGFloat imageViewHeight = CGRectGetHeight(self.imageView.bounds);
    if (imageViewHeight <= CGRectGetHeight(self.bounds)) return;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(scrollImageIfNeeded)
                                               object:nil];
    
    [self.scrollView.layer removeAllAnimations];
    
    if(!self.isTop)
        self.scrollView.contentOffset = CGPointZero;
    
    // animate the scrolling
    NSTimeInterval duration = imageViewHeight / self.speedOfScrolling;
    [UIView animateWithDuration:duration
                          delay:self.timeOfPause
                        options:self.animationOptions | UIViewAnimationOptionAllowUserInteraction
                     animations:^
     {
         CGPoint pointToMove;
         if (self.isTop) {
             self.isTop = NO;
             pointToMove = CGPointZero;
         }
         else {
             self.isTop = YES;
             pointToMove = CGPointMake(0, imageViewHeight - CGRectGetHeight(self.bounds));
         }
         self.scrollView.contentOffset = pointToMove;
     }
                     completion:^(BOOL finished)
     {
         if (finished) {
             [self performSelector:@selector(scrollImageIfNeeded) withObject:nil];
         }
     }];
}

- (void)updateSize {
    [self refreshImageView];
}

#pragma mark - Private

- (void)generalInit
{
    _scrollView                                = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.autoresizingMask               = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _scrollView.backgroundColor                = self.backgroundColor;
    _scrollView.showsVerticalScrollIndicator   = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.scrollEnabled                  = NO;
    
    [self addSubview:_scrollView];
    
    UIImageView* imageView = [[UIImageView alloc] init];
//    imageView.translatesAutoresizingMaskIntoConstraints = NO;
//    imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
//    imageView.contentMode = UIViewContentModeCenter;
    _imageView = imageView;

    [_scrollView addSubview:imageView];
    
    [self describeSrollableImageViewFrame];
    
    _speedOfScrolling = kDefaultPixelsPerSecond;
    _timeOfPause      = kDefaultPauseTime;
    _animationOptions = UIViewAnimationOptionCurveEaseInOut;
}

- (void)subscribeNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)refreshImageView
{
    self.isTop = NO;

    [self describeSrollableImageViewFrame];

    self.scrollView.contentOffset = CGPointZero;
    [self.scrollView.layer removeAllAnimations];

    if (CGRectGetHeight(self.imageView.bounds) > CGRectGetHeight(self.bounds)) {
        CGSize size = CGSizeMake(CGRectGetWidth(self.bounds),
                                 CGRectGetHeight(self.bounds) + CGRectGetHeight(self.imageView.bounds));
        self.scrollView.contentSize = size;

        //    TODO: A.S. Commented to avoid automaticaly scrolling image
        //    [self scrollImageIfNeeded];
    }
    else {
        self.scrollView.contentSize = self.bounds.size;

        [self.scrollView.layer removeAllAnimations];
    }
}

- (void)describeSrollableImageViewFrame
{
    if (!_imageView) return;
    
    CGRect frame      = _imageView.frame;
    frame.origin      = CGPointZero;
    _imageView.frame  = frame;
    [_imageView sizeToFit];
    
    CGFloat startX = (CGRectGetWidth(self.frame) - CGRectGetWidth(_imageView.frame))/2;
    CGFloat startY = (CGRectGetHeight(self.frame) - CGRectGetHeight(_imageView.frame))/2;
    if (startY<0) {
        startY=0;
    }
    
    frame             = CGRectMake(startX,
                                   startY,
                                   CGRectGetWidth(_imageView.frame),
                                   CGRectGetHeight(_imageView.frame));
    
    _imageView.frame  = frame;
    [_imageView sizeToFit];
}

#pragma mark - Selectors

- (void)applicationDidEnterForeground {
    [self scrollImageIfNeeded];
}

#pragma mark - Custom Setters and Getters

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self refreshImageView];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    [self refreshImageView];
}

@end

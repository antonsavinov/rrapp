//
//  RadioTableViewCell.m
//  RadioRecordApp
//
//  Created by Admin on 27.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RadioTableViewCell.h"

@interface RadioTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *radioStationNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioStationIcon;
@property (weak, nonatomic) IBOutlet UIView *tagNewView;

@end

#pragma mark - Init methods

@implementation RadioTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellForRadioStation:(RadioStation*)radioStation {
    self.radioStationNameLabel.text = radioStation.name;
    self.radioStationIcon.image = [radioStation radioStationIcon];
    self.tagNewView.hidden = !radioStation.isNew;
}

@end

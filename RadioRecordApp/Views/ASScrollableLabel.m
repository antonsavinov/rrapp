//
//  CustomScrollingLabel.m
//  RadioRecordApp
//
//  Created by Admin on 01.05.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "ASScrollableLabel.h"

#define kDefaultPixelsPerSecond 30
#define kDefaultPauseTime 1.f

@interface ASScrollableLabel ()

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UILabel*      label;
@property (nonatomic, assign) BOOL          isLeft;

@end

@implementation ASScrollableLabel

#pragma mark - Life cycle

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self generalInit];
        [self subscribeNotifications];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self generalInit];
        [self subscribeNotifications];
    }
    
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Public

- (void)scrollTextIfNeeded
{
    if (!self.text.length) return;
    
    CGFloat labelWidth = CGRectGetWidth(self.label.bounds);
    if (labelWidth <= CGRectGetWidth(self.bounds)) return;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollTextIfNeeded) object:nil];
    
    [self.scrollView.layer removeAllAnimations];
    if(!self.isLeft)
        self.scrollView.contentOffset = CGPointZero;
    
    // animate the scrolling
    NSTimeInterval duration = labelWidth / self.speedOfScrolling;
    [UIView animateWithDuration:duration
                          delay:self.timeOfPause
                        options:self.animationOptions | UIViewAnimationOptionAllowUserInteraction
                     animations:^
     {
         CGPoint pointToMove;
         if (self.isLeft)
         {
             self.isLeft = NO;
             pointToMove = CGPointZero;
             
         }
         else
         {
             self.isLeft = YES;
             pointToMove = CGPointMake(labelWidth - CGRectGetWidth(self.bounds), 0);
         }

         self.scrollView.contentOffset = pointToMove;
     }
                     completion:^(BOOL finished)
     {
         if (finished)
         {
             [self performSelector:@selector(scrollTextIfNeeded) withObject:nil];
         }
     }];
}

- (void)scrollTextIfNeededWithSpeed:(CGFloat)speedOfScrolling
                        timeOfPause:(NSTimeInterval)timeOfPause
                andAnimationOptions:(UIViewAnimationOptions)animationOptions
{
    _speedOfScrolling = speedOfScrolling;
    _timeOfPause      = timeOfPause;
    _animationOptions = animationOptions;
}

- (void)setScrollableTextColor:(UIColor*)scrollableTextColor
{
    _label.textColor = scrollableTextColor;
    
    [self refreshLabel];
}

- (void)setScrollableTextBackgroundColor:(UIColor*)scrollableTextBackgroundColor
{
    _scrollView.backgroundColor = scrollableTextBackgroundColor;
    
    [self refreshLabel];
}

#pragma mark - Private

- (void)generalInit
{
    _scrollView                                = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.autoresizingMask               = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _scrollView.backgroundColor                = self.backgroundColor;
    _scrollView.showsVerticalScrollIndicator   = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.scrollEnabled                  = NO;
    [self addSubview:_scrollView];
    
    UILabel *label         = [UILabel new];
    label.backgroundColor  = [UIColor clearColor];
    label.autoresizingMask = self.autoresizingMask;
    label.text             = self.text;
    label.textAlignment    = self.textAlignment;
    label.textColor        = self.textColor;
    label.numberOfLines    = self.numberOfLines;
    label.lineBreakMode    = self.lineBreakMode;
    label.font             = self.font;
    [self describeSrollableLabelFrame];
    _label            = label;
    
    [_scrollView addSubview:label];
    
    _speedOfScrolling = kDefaultPixelsPerSecond;
    _timeOfPause      = kDefaultPauseTime;
    _animationOptions = UIViewAnimationOptionCurveEaseInOut;

    self.userInteractionEnabled = NO;
    self.backgroundColor = [UIColor clearColor];
    self.textColor = [UIColor clearColor];
    self.clipsToBounds = YES;
}

- (void)subscribeNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)refreshLabel
{
    self.isLeft = NO;
    
    [self describeSrollableLabelFrame];
    
    self.scrollView.contentOffset = CGPointZero;
    [self.scrollView.layer removeAllAnimations];
    
    if (CGRectGetWidth(self.label.bounds) > CGRectGetWidth(self.bounds)) {
        CGSize size;
        size.width = CGRectGetWidth(self.label.bounds) + CGRectGetWidth(self.bounds);
        size.height = CGRectGetHeight(self.bounds);
        self.scrollView.contentSize = size;
        
        [self scrollTextIfNeeded];
    }
    else
    {
        self.scrollView.contentSize = self.bounds.size;
        self.label.frame = self.bounds;

        [self.scrollView.layer removeAllAnimations];
    }
}

- (void)describeSrollableLabelFrame
{
    CGRect frame      = _label.frame;
    frame.origin      = CGPointZero;
    frame.size.height = CGRectGetHeight(self.bounds);
    _label.frame      = frame;
    _label.center     = CGPointMake(CGRectGetMidX(self.label.frame), roundf(CGRectGetMidY(self.frame) - CGRectGetMinY(self.frame)));
    
    [_label sizeToFit];
}

#pragma mark - Selectors

- (void)applicationDidEnterForeground {
    [self scrollTextIfNeeded];
}

#pragma mark - Custom Setters and Getters

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self refreshLabel];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    [self refreshLabel];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    _label.font = font;
}

- (void)setText:(NSString *)text
{
    _label.text = text;
    
    [self refreshLabel];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    _label.textAlignment = textAlignment;
    
    [self refreshLabel];
}

@end

//
//  ActivityIndicator.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 16/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIImageView

@end

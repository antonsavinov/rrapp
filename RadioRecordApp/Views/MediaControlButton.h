//
//  MediaControlButton.h
//  PlayButtonProject
//
//  Created by Anton Savinov on 13/03/2018.
//  Copyright © 2018 Anton Savinov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MediaControlIcon) {
    MediaControlIconPlay = 0,
    MediaControlIconLoading,
    MediaControlIconPause
};

@interface MediaControlButton : UIButton

@property (nonatomic, assign) MediaControlIcon icon;

@end

//
//  NavigationBarView.h
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavigationBarViewDelegate;

typedef NS_ENUM(NSUInteger, NavigationBarItem) {
    NavigationBarItemUnknown = 0,
    NavigationBarItemBack
};

@interface NavigationBarView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) id<NavigationBarViewDelegate>delegate;

@property (assign, nonatomic) NavigationBarItem leftBarItem;
@property (assign, nonatomic) NavigationBarItem rightBarItem;

@end

@protocol NavigationBarViewDelegate <NSObject>

@optional
-(void)navigationBarView:(NavigationBarView*)navigationBarView wasClickedLeftItem:(UIButton*)leftButton;
-(void)navigationBarView:(NavigationBarView*)navigationBarView wasClickedRightItem:(UIButton*)rightButton;

@end
//
//  ASScrollableLabel.h
//  RadioRecordApp
//
//  Created by Savinov Anton on 01.05.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASScrollableLabel : UILabel

@property (nonatomic, assign) CGFloat                speedOfScrolling;
@property (nonatomic, assign) NSTimeInterval         timeOfPause;
@property (nonatomic, assign) UIViewAnimationOptions animationOptions;


- (void)scrollTextIfNeeded;
- (void)scrollTextIfNeededWithSpeed:(CGFloat)speedOfScrolling timeOfPause:(NSTimeInterval)timeOfPause andAnimationOptions:(UIViewAnimationOptions)animationOptions;

- (void)setScrollableTextColor:(UIColor*)scrollableTextColor;
- (void)setScrollableTextBackgroundColor:(UIColor*)scrollableTextBackgroundColor;

@end

//
//  RadioTableViewCell.h
//  RadioRecordApp
//
//  Created by Admin on 27.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RadioStation.h"

@interface RadioTableViewCell : UITableViewCell

- (void)setupCellForRadioStation:(RadioStation*)radioStation;

@end

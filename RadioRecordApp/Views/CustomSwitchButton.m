//
//  CustomSwitchButton.m
//  TouchTunesv3
//
//  Created by Alexey Vasilyev on 26.11.15.
//  Copyright © 2015 TouchTunes. All rights reserved.
//

#import "CustomSwitchButton.h"

static const float kSwitchBorderWidth = 3.f;
static const float kSwitchAnimationTime = 0.3f;

@interface CustomSwitchButton ()

@property(strong, nonatomic) UIView*   hollowView;
@property(strong, nonatomic) UIView*   hollowEraser;
@property(strong, nonatomic) UIView*   activePart;
@property(strong, nonatomic) UIButton* actionButton;
@property(strong, nonatomic) UIColor*  activeColor;
@property(strong, nonatomic) UIColor*  disabledColor;
@property(strong, nonatomic) UILabel* insideTextLabel;

@end

@implementation CustomSwitchButton

#pragma mark - Lyfe cycle

-(instancetype)init
{
    if (self = [super init])
    {
        [self generalInit];
    }
    return self;
}

-(instancetype)initWithCoder:(nonnull NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self generalInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self generalInit];
    }
    return self;
}

#pragma mark - Private

- (void)generalInit
{
    self.activeColor = [UIColor colorWithRed:223.f/255.f green:33.f/255.f blue:133.f/255.f alpha:1.f];
    self.disabledColor = [UIColor darkGrayColor];
    
    self.hollowView = [UIView new];
    self.hollowView.backgroundColor = self.activeColor;
    
    self.hollowEraser = [UIView new];
    self.hollowEraser.backgroundColor = [UIColor colorWithRed:33.f/255.f green:33.f/255.f blue:33.f/255.f alpha:1.f];
    
    self.activePart = [UIView new];
    self.activePart.backgroundColor = self.disabledColor;
    //    self.activePart.layer.shadowColor = [UIColor colorWithRed:155.f/255.f green:33.f/255.f blue:103.f/255.f alpha:1.f].CGColor;
    //    self.activePart.layer.shadowOpacity = 0.5f;
    
    self.actionButton = [UIButton new];
    [self.actionButton addTarget:self action:@selector(switchPressed) forControlEvents:UIControlEventTouchUpInside];

    self.insideTextLabel = [UILabel new];
    self.insideTextLabel.backgroundColor = [UIColor clearColor];
    self.insideTextLabel.textColor = [UIColor whiteColor];
    self.insideTextLabel.font = self.textFont;
    self.insideTextLabel.textAlignment = NSTextAlignmentCenter;
    self.insideTextLabel.text = _selected ? self.selectedStateText : self.deselectedStateText;

    [self addSubview:self.hollowView];
    [self addSubview:self.hollowEraser];
    [self addSubview:self.activePart];
    [self addSubview:self.actionButton];
    [self addSubview:self.insideTextLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    
    self.hollowView.frame = self.bounds;
    self.hollowView.layer.cornerRadius = CGRectGetHeight(self.hollowView.frame) * 0.5f;
    
    self.hollowEraser.frame = CGRectMake(kSwitchBorderWidth, kSwitchBorderWidth,
                                         width-kSwitchBorderWidth*2, height-kSwitchBorderWidth*2);
    self.hollowEraser.layer.cornerRadius = CGRectGetHeight(self.hollowEraser.frame) * 0.5f;
    
    if (self.selected) {
        self.activePart.frame = CGRectMake(width - width*0.5f - kSwitchBorderWidth*2, kSwitchBorderWidth*2,
                                           width*0.5f, height-kSwitchBorderWidth*4);
    } else {
        self.activePart.frame = CGRectMake(kSwitchBorderWidth*2, kSwitchBorderWidth*2,
                                           width*0.5f, height-kSwitchBorderWidth*4);
    }

    self.insideTextLabel.frame = self.activePart.frame;

    CGFloat activePartHeight = CGRectGetHeight(self.activePart.frame);
    self.activePart.layer.cornerRadius = activePartHeight * 0.5f;
    self.activePart.layer.shadowOffset = CGSizeMake(0, activePartHeight * 0.5f);
    
    self.actionButton.frame = self.bounds;
}

- (void)switchPressed
{
    self.selected = !self.selected;
    
    if ([self.delegate respondsToSelector:@selector(switchButton:wasChangedState:)]) {
        [self.delegate switchButton:self wasChangedState:self.selected];
    }
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;

    [self performSelector:@selector(changeTextAnimated) withObject:nil afterDelay:kSwitchAnimationTime/50];
    [self changeActivePartPositionAnimated];
}

- (void)changeTextAnimated {

    [UIView transitionWithView:self.insideTextLabel
                      duration:kSwitchAnimationTime
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.insideTextLabel.text = _selected ? self.selectedStateText : self.deselectedStateText;
                    } completion:^(BOOL finished) {

                    }];
}

- (void)changeActivePartPositionAnimated {

    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);

    [UIView animateWithDuration:kSwitchAnimationTime
                     animations:^{

                         UIColor* color = _selected ? self.activeColor : self.disabledColor;
                         CGFloat newX = _selected ? (width - width*0.5f - kSwitchBorderWidth*2) : kSwitchBorderWidth*2;

                         self.activePart.backgroundColor = color;
                         self.activePart.frame = CGRectMake(newX, kSwitchBorderWidth*2,
                                                            width*0.5f, height-kSwitchBorderWidth*4);

                         self.insideTextLabel.frame = self.activePart.frame;
                         
                     } completion:nil];
}

#pragma mark - Custom setters/getters

- (void)setTextFont:(UIFont *)textFont {
    self.insideTextLabel.font = textFont;
}

@end

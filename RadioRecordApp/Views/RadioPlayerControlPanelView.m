//
//  RadioPlayerControlPanelView.m
//  RadioRecordApp
//
//  Created by Admin on 04.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "RadioPlayerControlPanelView.h"
#import "UIView+Autolayout.h"

#import "UIColor+Utils.h"

@interface RadioPlayerControlPanelView ()

@property (weak, nonatomic) IBOutlet UILabel *qualityLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordButtonLabel;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *qualityButton;
@property (weak, nonatomic) IBOutlet MediaControlButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *top100Button;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@end

@implementation RadioPlayerControlPanelView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self load];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self load];
    }
    return self;
}

#pragma mark - Private

- (void)load {
    UIView *xibView =
    [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"RadioPlayerControlPanelView" owner:self options:nil] firstObject];
    [self addSubview:xibView];
    xibView.translatesAutoresizingMaskIntoConstraints = NO;
    [xibView matchToParent];
    
    self.recordButton.layer.cornerRadius = 4.f;
    self.recordButton.layer.borderWidth = 1.f;
    self.recordButton.layer.borderColor = [[UIColor radioRecordGrayColor] CGColor];
    
    self.qualityButton.layer.cornerRadius = 4.f;
    self.top100Button.layer.cornerRadius = 4.f;
    
//    self.historyButton.layer.cornerRadius = 4.f;
//    self.historyButton.layer.borderWidth = 1.f;
//    self.historyButton.layer.borderColor = [[UIColor radioRecordGrayColor] CGColor];

    self.playButton.icon = MediaControlIconPlay;
}

- (void)updateQualityLabel {
    switch (_audioQuality) {
        case ControlPanelAudioQualityLow:
            _qualityLabel.text = Localized(@"IDS_RADIO_PLAY_CONTROL_PANEL_VIEW_QUALITY_LOW");
            break;
        case ControlPanelAudioQualityMedium:
            _qualityLabel.text = Localized(@"IDS_RADIO_PLAY_CONTROL_PANEL_VIEW_QUALITY_MED");
            break;
        case ControlPanelAudioQualityHigh:
            _qualityLabel.text = Localized(@"IDS_RADIO_PLAY_CONTROL_PANEL_VIEW_AUDIO_QUALITY_HIGH");
            break;
    }
}

#pragma mark - IBActions

- (IBAction)playButtonAction:(id)sender {

    if (self.playButton.icon == MediaControlIconPlay) {
        self.playButton.icon = MediaControlIconLoading;
    } /*else if (self.playButton.icon == MediaControlIconLoading) {
        self.playButton.icon = MediaControlIconPause;
    }*/ else {
        self.playButton.icon = MediaControlIconPlay;
    }

    [self.delegate radioPlayerControlPanelView:self mediaControlChangedIcon:self.playButton.icon];
}

- (IBAction)qualityButtonTouchDownAction:(id)sender {
    self.qualityButton.backgroundColor = [UIColor radioRecordDarkPinColor];
}

- (IBAction)qualityButtonAction:(id)sender {
    self.qualityButton.backgroundColor = [UIColor radioRecordGrayColor];
    
    if (self.audioQuality == ControlPanelAudioQualityHigh) {
        self.audioQuality = ControlPanelAudioQualityLow;
    } else {
        self.audioQuality++;
    }
    if ([self.delegate respondsToSelector:@selector(radioPlayerControlPanelView:audioQualityChangedState:)]) {
        [self.delegate radioPlayerControlPanelView:self audioQualityChangedState:self.audioQuality];
    }
}

- (IBAction)recordButtonAction:(id)sender {
    self.recordButtonState = (self.recordButtonState == ControlPanelRecordButtonStateNotRecording) ? ControlPanelRecordButtonStateRecording : ControlPanelRecordButtonStateNotRecording;
    if ([self.delegate respondsToSelector:@selector(radioPlayerControlPanelView:recordButtonChangedState:)]) {
        [self.delegate radioPlayerControlPanelView:self recordButtonChangedState:self.recordButtonState];
    }
    
}
- (IBAction)top100Action:(id)sender {
    if ([self.delegate respondsToSelector:@selector(top100TappedOnRadioPlayerControlPanelView:)]) {
        [self.delegate top100TappedOnRadioPlayerControlPanelView:self];
    }
}

- (IBAction)historyAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(historyTappedOnRadioPlayerControlPanelView:)]) {
        [self.delegate historyTappedOnRadioPlayerControlPanelView:self];
    }
}

#pragma mark - Setters and Getters

- (void)setAudioQuality:(ControlPanelAudioQuality)audioQuality {
    _audioQuality = audioQuality;
    
    [self updateQualityLabel];
}

- (void)setMediaControlIcon:(MediaControlIcon)mediaControlIcon {
    _mediaControlIcon = mediaControlIcon;

    self.playButton.icon = mediaControlIcon;
}

@end

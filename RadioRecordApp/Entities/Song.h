//
//  Song.h
//  RadioRecordApp
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

#import "iTunesSearchItem.h"
#import "LastFMTrackItem.h"
#import "SongModel+CoreDataProperties.h"

@interface Song : NSObject

@property (nonatomic, copy, readonly) NSString* artistName;
@property (nonatomic, copy, readonly) NSString* title;
@property (nonatomic, copy, readonly) NSString* primaryGenreName;
@property (nonatomic, copy, readonly) NSString* releaseDate;
@property (nonatomic, copy, readonly) NSString* trackViewURL;
@property (nonatomic, copy, readonly) NSString* albumSmallImageLink;
@property (nonatomic, copy, readonly) NSString* albumMediumImageLink;
@property (nonatomic, copy, readonly) NSString* albumLargeImageLink;
@property (nonatomic, copy, readonly) NSString* albumExtraLargeImageLink;
@property (nonatomic, copy, readonly) NSString* albumArtwork100;
@property (nonatomic, copy, readonly) NSString* albumArtwork300;
@property (nonatomic, copy, readonly) NSString* albumArtwork600;
@property (nonatomic, readonly) int64_t trackId;
@property (nonatomic, readonly) int64_t artistId;
@property (nonatomic, readonly) double duration;

- (instancetype)instatiateFromMetaData:(AVMetadataItem*)metaDataItem;
- (instancetype)instatiateFromITunes:(iTunesSearchItem*)iTunesSearchItem;
- (instancetype)instatiateFromSongModel:(SongModel*)songModel;

+ (Song*)songFromMetaData:(AVMetadataItem*)metaDataItem;
+ (Song*)songFromITunes:(iTunesSearchItem*)iTunesSearchItem;
+ (Song*)songFromSongModel:(SongModel*)songModel;

- (BOOL)isValid;
- (void)addInfoFromITunesSearch:(iTunesSearchItem*)searchItem;
- (void)addInfoFromLastFmTrackItem:(LastFMTrackItem*)lastFMTrackItem;
- (void)proceedDuration:(double)duration;

@end

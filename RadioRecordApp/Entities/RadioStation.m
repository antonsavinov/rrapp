//
//  RadioStation.m
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RadioStation.h"

static NSString* const kUnderscoreSymbol = @"_";
static NSString* const kAACAudioType     = @"aac";

@implementation RadioStation

#pragma mark - Life cycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

+ (instancetype)radioStationFromJSONObject:(NSDictionary*)jsonObject
{
    RadioStation* radioStation = [RadioStation new];
    radioStation.identifier    = jsonObject[@"identifier"] ? jsonObject[@"identifier"] : @"";
    radioStation.name          = jsonObject[@"name"] ? jsonObject[@"name"] : @"";
    radioStation.icon          = jsonObject[@"radio_icon"] ? jsonObject[@"radio_icon"] : @"";
    radioStation.urlString     = jsonObject[@"url"] ? jsonObject[@"url"] : @"";
    radioStation.isNew         = jsonObject[@"new_station"] ? ((NSNumber*)(jsonObject[@"new_station"])).boolValue : NO;
    
    return radioStation;
}

+ (NSArray<RadioStation*>*)radioStantionsFromJSONObjects:(NSArray<NSDictionary*>*)jsonObjects
{
    NSMutableArray<RadioStation*>* radioStations = [NSMutableArray new];
    for (NSDictionary* radioStationJSONObject in jsonObjects)
    {
        [radioStations addObject:[RadioStation radioStationFromJSONObject:radioStationJSONObject]];
    }
    
    return [radioStations copy];
}

#pragma mark - Public

- (UIImage *)radioStationIcon
{
    UIImage* radioIcon = [UIImage imageNamed:self.icon];
    return radioIcon;
}

- (NSString*)composeURLWithStreamAudioType:(RadioStationStreamType)audioStreamType
                            andQualityType:(RadioStationQualityType)qualityType
{
    NSMutableString* streamedURLString = [self.urlString mutableCopy];
    
    if (audioStreamType == RadioStationStreamTypeMP3)
    {
        NSNumber* quality = qualityType == RadioStationQualityTypeLow ? @(128) : @(320);
        [streamedURLString appendFormat:@"%@%@", kUnderscoreSymbol, quality.stringValue];
    }
    else
    {
        NSNumber* quality = qualityType == RadioStationQualityTypeLow ? @(32) : @(64);
        [streamedURLString appendFormat:@"%@%@%@%@", kUnderscoreSymbol, kAACAudioType, kUnderscoreSymbol,quality.stringValue];
    }
    
    return streamedURLString;
}

@end

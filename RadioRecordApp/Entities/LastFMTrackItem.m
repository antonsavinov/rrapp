//
//  LastFMTrackItem.m
//  RadioRecordApp
//
//  Created by Admin on 26.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "LastFMTrackItem.h"

@implementation LastFMTrackItem

#pragma mark - Lyfe cycle

- (instancetype)instatiateFromJson:(NSDictionary*)json {
    
    if (self == [super init]) {
        _trackTitle = json[@"name"];
        _artistName = json[@"artist"][@"name"];
        _albumTitle = json[@"album"][@"title"];
        
        NSArray<NSDictionary<NSString*, NSString*>*>* imagesData = json[@"album"][@"image"];
        for (NSDictionary* imageData in imagesData) {
            if ([imageData[@"size"] isEqualToString:@"small"]) {
                _albumSmallImageLink = imageData[@"#text"];
            } else if ([imageData[@"size"] isEqualToString:@"medium"]) {
                _albumMediumImageLink = imageData[@"#text"];
            } else if ([imageData[@"size"] isEqualToString:@"large"]) {
                _albumLargeImageLink = imageData[@"#text"];
            } else if ([imageData[@"size"] isEqualToString:@"extralarge"]) {
                _albumExtraLargeImageLink = imageData[@"#text"];
            }
        }
    }
    
    return self;
}

+ (LastFMTrackItem*)createItemFromJSON:(NSDictionary*)json {
    LastFMTrackItem* lastFMTrackItem = [[LastFMTrackItem alloc] instatiateFromJson:json];
    
    return lastFMTrackItem;
}

@end

//
//  iTunesSearchItem.h
//  RadioRecordApp
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iTunesSearchItem : NSObject

@property (nonatomic, readonly) NSUInteger artistId;
@property (nonatomic, readonly) NSString* artistName;
@property (nonatomic, readonly) NSString* artworkUrl600;
@property (nonatomic, readonly) NSString* artworkUrl300;
@property (nonatomic, readonly) NSString* artworkUrl100;
@property (nonatomic, readonly) NSString* artworkUrl60;
@property (nonatomic, readonly) NSString* artworkUrl30;
@property (nonatomic, readonly) NSString* primaryGenreName;
@property (nonatomic, readonly) NSString* kind;
@property (nonatomic, readonly) NSString* releaseDate;
@property (nonatomic, readonly) NSUInteger trackId;
@property (nonatomic, readonly) NSString* trackName;
@property (nonatomic, readonly) NSString* trackViewUrl;

- (instancetype)instatiateFromJson:(NSDictionary*)json;

+ (iTunesSearchItem*)createItemFromJSON:(NSDictionary*)json;
+ (NSArray<iTunesSearchItem*>*)createItemsFromJSONs:(NSArray<NSDictionary*>*)jsons;

+ (iTunesSearchItem*)firstSongfromCollection:(NSArray<iTunesSearchItem*>*)items;

@end
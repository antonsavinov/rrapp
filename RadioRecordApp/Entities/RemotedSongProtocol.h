//
//  RemotedSongProtocol.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 15/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RemotedSongProtocol <NSObject>

@property (strong, nonatomic) NSString* artistName;
@property (strong, nonatomic) NSString* songTitle;
@property (strong, nonatomic) NSString* remoteUrlForPlaying;
@property (assign, nonatomic) double duration;

@end

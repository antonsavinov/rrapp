//
//  SongModel+CoreDataProperties.h
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "SongModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SongModel (CoreDataProperties)

+ (NSFetchRequest *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *artistName;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *primaryGenreName;
@property (nullable, nonatomic, copy) NSString *releaseDate;
@property (nullable, nonatomic, copy) NSString *trackViewURL;
@property (nullable, nonatomic, copy) NSString *albumSmallImageLink;
@property (nullable, nonatomic, copy) NSString *albumMediumImageLink;
@property (nullable, nonatomic, copy) NSString *albumLargeImageLink;
@property (nullable, nonatomic, copy) NSString *albumExtraLargeImageLink;
@property (nullable, nonatomic, copy) NSString *albumArtwork100;
@property (nonatomic) int64_t trackId;
@property (nonatomic) int64_t artistId;
@property (nullable, nonatomic, copy) NSString *albumArtwork300;
@property (nullable, nonatomic, copy) NSString *albumArtwork600;

@end

NS_ASSUME_NONNULL_END

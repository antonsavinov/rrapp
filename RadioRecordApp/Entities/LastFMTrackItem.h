//
//  LastFMTrackItem.h
//  RadioRecordApp
//
//  Created by Admin on 26.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LastFMTrackItem : NSObject

@property (nonatomic, readonly) NSString* trackTitle;
@property (nonatomic, readonly) NSString* artistName;
@property (nonatomic, readonly) NSString* albumTitle;
@property (nonatomic, readonly) NSString* albumSmallImageLink;
@property (nonatomic, readonly) NSString* albumMediumImageLink;
@property (nonatomic, readonly) NSString* albumLargeImageLink;
@property (nonatomic, readonly) NSString* albumExtraLargeImageLink;

+ (LastFMTrackItem*)createItemFromJSON:(NSDictionary*)json;
- (instancetype)instatiateFromJson:(NSDictionary*)json;

@end

//
//  HistorySong.h
//  RadioRecordApp
//
//  Created by Anton Savinov on 05/03/2018.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RemotedSongProtocol.h"
#import "HistorySongProtocol.h"

@interface HistorySong : NSObject <RemotedSongProtocol, HistorySongProtocol>

@property (strong, nonatomic) NSString* artistName;
@property (strong, nonatomic) NSString* songTitle;
@property (strong, nonatomic) NSString* remoteUrlForPlaying;
@property (assign, nonatomic) double duration;
@property (strong, nonatomic) NSString* startTime;

@end

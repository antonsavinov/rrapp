//
//  Song.m
//  RadioRecordApp
//
//  Created by Admin on 18.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "Song.h"

@implementation Song

#pragma mark - Lyfe Cycle

- (instancetype)instatiateFromMetaData:(AVMetadataItem*)metaDataItem {
    if (self == [super init]) {
        
        NSString* value = metaDataItem.stringValue;        
        NSMutableArray* components = [[value componentsSeparatedByString:@" - "] mutableCopy];
        
        if (components.count != 2) return nil;
        
        for (NSUInteger index = 0; index < components.count; index++)
        {
            NSData* firstData = [components[index] dataUsingEncoding:NSISOLatin1StringEncoding];
            NSString* covertedString = [[NSString alloc] initWithData:firstData encoding:NSUTF8StringEncoding];
            
            components[index] = covertedString;
        }
        
        NSString* artistName = [components firstObject];
        NSString* songTitle = [components lastObject];
        
        _artistName = artistName;
        _title = songTitle;
        
        _trackId = NSNotFound;
        _artistId = NSNotFound;
        
    }
    
    return self;
}
- (instancetype)instatiateFromITunes:(iTunesSearchItem*)iTunesSearchItem {
    if (self == [super init]) {
        
        _trackId = iTunesSearchItem.trackId;
        _artistId = iTunesSearchItem.artistId;
        _title = iTunesSearchItem.trackName;
        _artistName = iTunesSearchItem.artistName;
        _primaryGenreName = iTunesSearchItem.primaryGenreName;
        _releaseDate = iTunesSearchItem.releaseDate;
        _trackViewURL = iTunesSearchItem.trackViewUrl;
        
    }
    
    return self;
}

- (instancetype)instatiateFromSongModel:(SongModel *)songModel {
    if (self == [super init]) {
        _trackId = songModel.trackId;
        _artistId = songModel.artistId;
        _title = songModel.title;
        _artistName = songModel.artistName;
        _primaryGenreName = songModel.primaryGenreName;
        _releaseDate = songModel.releaseDate;
        _trackViewURL = songModel.trackViewURL;
        _albumSmallImageLink = songModel.albumSmallImageLink;
        _albumMediumImageLink = songModel.albumMediumImageLink;
        _albumLargeImageLink = songModel.albumLargeImageLink;
        _albumExtraLargeImageLink = songModel.albumExtraLargeImageLink;
        _albumArtwork100 = songModel.albumArtwork100;
        _albumArtwork300 = songModel.albumArtwork300;
        _albumArtwork600 = songModel.albumArtwork600;
    }
    return self;
}

+ (Song*)songFromMetaData:(AVMetadataItem*)metaDataItem {
    
    Song* song = [[Song alloc] instatiateFromMetaData:metaDataItem];
    
    return song;
}

+(Song *)songFromITunes:(iTunesSearchItem *)iTunesSearchItem {
    
    Song* song = [[Song alloc] instatiateFromITunes:iTunesSearchItem];
    
    return song;
}

+ (Song *)songFromSongModel:(SongModel *)songModel {
    Song *song = [[Song alloc] instatiateFromSongModel:songModel];
    
    return song;
}

#pragma mark - Public

- (BOOL)isValid {
    if (self.artistName && self.title) {
        return YES;
    } else {
        return NO;
    }
}

- (void)addInfoFromITunesSearch:(iTunesSearchItem*)searchItem {
    if (!searchItem) return;
    
    _trackId = searchItem.trackId;
    _artistId = searchItem.artistId;
    _primaryGenreName = searchItem.primaryGenreName;
    _releaseDate = searchItem.releaseDate;
    _trackViewURL = searchItem.trackViewUrl;
    _albumArtwork100 = searchItem.artworkUrl100;
    _albumArtwork300 = searchItem.artworkUrl300;
    _albumArtwork600 = searchItem.artworkUrl600;
}

- (void)addInfoFromLastFmTrackItem:(LastFMTrackItem*)lastFMTrackItem {
    if (!lastFMTrackItem) return;
    
    _albumSmallImageLink = lastFMTrackItem.albumSmallImageLink;
    _albumMediumImageLink = lastFMTrackItem.albumMediumImageLink;
    _albumLargeImageLink = lastFMTrackItem.albumLargeImageLink;
    _albumExtraLargeImageLink = lastFMTrackItem.albumExtraLargeImageLink;
}

- (void)proceedDuration:(double)duration {
    _duration = duration;
}

@end

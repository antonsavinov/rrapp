//
//  SongModel+CoreDataProperties.m
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "SongModel+CoreDataProperties.h"

@implementation SongModel (CoreDataProperties)

+ (NSFetchRequest *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SongModel"];
}

@dynamic artistName;
@dynamic title;
@dynamic primaryGenreName;
@dynamic releaseDate;
@dynamic trackViewURL;
@dynamic albumSmallImageLink;
@dynamic albumMediumImageLink;
@dynamic albumLargeImageLink;
@dynamic albumExtraLargeImageLink;
@dynamic albumArtwork100;
@dynamic trackId;
@dynamic artistId;
@dynamic albumArtwork300;
@dynamic albumArtwork600;

@end

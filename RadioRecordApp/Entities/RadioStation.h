//
//  RadioStation.h
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    RadioStationStreamTypeMP3,
    RadioStationStreamTypeAAC
} RadioStationStreamType;

typedef enum : NSUInteger {
    RadioStationQualityTypeLow,
    RadioStationQualityTypeHigh
} RadioStationQualityType;

@interface RadioStation : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, assign) BOOL      isNew;
@property (nonatomic, strong) NSString* icon;
@property (nonatomic, strong) NSString* urlString;

+ (instancetype)radioStationFromJSONObject:(NSDictionary*)jsonObject;
+ (NSArray<RadioStation*>*)radioStantionsFromJSONObjects:(NSArray<NSDictionary*>*)jsonObjects;

- (UIImage*)radioStationIcon;

- (NSString*)composeURLWithStreamAudioType:(RadioStationStreamType)audioStreamType
                             andQualityType:(RadioStationQualityType)qualityType;

@end

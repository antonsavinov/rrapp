//
//  iTunesSearchItem.m
//  RadioRecordApp
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "iTunesSearchItem.h"

@implementation iTunesSearchItem

#pragma mark - Life cycle

- (instancetype)instatiateFromJson:(NSDictionary*)json {
    
    if (self == [super init]) {
        
        _artistId = json[@"artistId"] ? [json[@"artistId"] integerValue] : NSNotFound;
        _artistName = json[@"artistName"] ? json[@"artistName"] : nil;
        _artworkUrl100 = json[@"artworkUrl100"] ? json[@"artworkUrl100"] : nil;
        _artworkUrl60 = json[@"artworkUrl60"] ? json[@"artworkUrl60"] : nil;
        _artworkUrl30 = json[@"artworkUrl30"] ? json[@"artworkUrl30"] : nil;
        _primaryGenreName = json[@"primaryGenreName"] ? json[@"primaryGenreName"] : nil;
        _kind = json[@"kind"] ? json[@"kind"] : nil;
        _releaseDate = json[@"releaseDate"] ? json[@"releaseDate"] : nil;
        _trackId = json[@"trackId"] ? [json[@"trackId"] integerValue] : NSNotFound;
        _trackName = json[@"trackName"] ? json[@"trackName"] : nil;
        _trackViewUrl = json[@"trackViewUrl"] ? json[@"trackViewUrl"] : nil;
        
        _artworkUrl300 = [_artworkUrl100 stringByReplacingOccurrencesOfString:@"100" withString:@"300"];
        _artworkUrl600 = [_artworkUrl100 stringByReplacingOccurrencesOfString:@"100" withString:@"600"];
    }
    
    return self;
}

+ (iTunesSearchItem*)createItemFromJSON:(NSDictionary*)json {
    
    iTunesSearchItem* searchItem = [[iTunesSearchItem alloc] instatiateFromJson:json];
    
    return searchItem;
}


+ (NSArray<iTunesSearchItem*>*)createItemsFromJSONs:(NSArray<NSDictionary*>*)jsons {
    
    NSMutableArray* searchItems = [NSMutableArray array];
    
    for (NSDictionary* json in jsons) {
        
        iTunesSearchItem* searchItem = [iTunesSearchItem createItemFromJSON:json];
        [searchItems addObject:searchItem];
        
    }
    
    return searchItems;
}

#pragma mark - Public

+ (iTunesSearchItem*)firstSongfromCollection:(NSArray<iTunesSearchItem*>*)items {
    
    for (iTunesSearchItem* item in items) {
        if ([item.kind isEqualToString:@"song"]) {
            return item;
        }
    }
    
    return nil;
}

@end

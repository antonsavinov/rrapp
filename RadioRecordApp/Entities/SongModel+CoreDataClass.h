//
//  SongModel+CoreDataClass.h
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SongModel : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SongModel+CoreDataProperties.h"

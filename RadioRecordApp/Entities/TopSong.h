//
//  TopSong.h
//  RadioRecordApp
//
//  Created by Admin on 06.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RemotedSongProtocol.h"

@interface TopSong : NSObject <RemotedSongProtocol>

@property (strong, nonatomic) NSString* artistName;
@property (strong, nonatomic) NSString* songTitle;
@property (strong, nonatomic) NSString* remoteUrlForPlaying;
@property (assign, nonatomic) double duration;

@end

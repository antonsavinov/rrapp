//
//  SongLocalStorage.h
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//
@class Song;

@interface SongLocalStorage : NSObject

+ (void)saveSong:(Song*)song;
+ (Song*)retriveSongByTitle:(NSString*)title andArtistName:(NSString*)artistName;

@end

//
//  PlayerManager.h
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

@class Song;

@protocol MediaPlayerManagerDelegate;

@interface MediaPlayerManager : NSObject

+ (instancetype)sharedInstance;

- (void)setDelegate:(id <MediaPlayerManagerDelegate>)delegate;
- (void)playMusicByURL:(NSString*)urlString;
- (void)stopPlaying;
- (BOOL)isPlayingNow;
- (BOOL)isAlreadyPlayingByURL:(NSString*)urlString;

- (void)startObservingStreamInfo;
- (void)stopObservingStreamInfo;
- (void)restartObservingStreamInfo;

- (CGFloat)volume;

@end

@protocol MediaPlayerManagerDelegate <NSObject>

- (void)mediaPlayerStreamBecameAvailable;
- (void)mediaPlayerCanNotPlayRadioWithError:(NSError*)error;
- (void)streamInfoLoadedWithSong:(Song*)song;
- (void)streamInfoNotLoadedWithError:(NSError*)error;

@optional
- (void)mediaPlayerFinishedPlaying;

@end

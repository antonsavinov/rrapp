//
//  ImageManager.m
//  RadioRecordApp
//
//  Created by Admin on 12.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIImage.h>

#import "ImageManager.h"

#import "NetworkManager.h"

@interface ImageManager () <NetworkManagerDelegate>

@property (nonatomic, weak) id<ImageManagerDelegate> delegate;
@property (nonatomic, strong) NetworkManager* networkManager;

@property (nonatomic, strong) NSString* cacheDirectoryPath;
@property (nonatomic, strong) NSString* currentImageKey;

@end

@implementation ImageManager

- (instancetype)init {
    if (self = [super init]) {
        
        _networkManager = [[NetworkManager alloc] init];
        
        [self initializeCachedDirectory];
        [self initializeCachedImages];
    }
    
    return self;
}

#pragma mark - Public

- (void)downloadImageByURL:(NSURL*)url delegate:(id<ImageManagerDelegate>)delegate {
    
    self.delegate = delegate;
    
    self.currentImageKey = url.path;
    UIImage* image = [self cachedImageByKey:self.currentImageKey];
    if (image) {
        if ([self.delegate respondsToSelector:@selector(imageManagerDownloadedImage:)]) {
            [self.delegate imageManagerDownloadedImage:image];
        }
    } else {
        [self.networkManager downloadImageWithURL:url delegate:self];
    }
}

- (void)cacheImage:(UIImage*)image byKey:(NSString*)key {
    if (!image || !key) return;
    
    NSString* cachedImageName = nil;
    
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    
    if ([key hasPrefix:@"/"]) {
        cachedImageName = [NSString stringWithFormat:@"%@%@", self.cacheDirectoryPath, key];
    } else {
        cachedImageName = [NSString stringWithFormat:@"%@/%@", self.cacheDirectoryPath, key];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachedImageName]) {
        
        //TODO: A.S.: Make multiple implementation for JPEG and PNG
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        
        if ([imageData writeToFile:cachedImageName atomically:YES]) {
            RRLog(@"Image was saved successfully by path %@", cachedImageName);
        }
    }
}

- (UIImage*)cachedImageByKey:(NSString*)key {
    
    NSString* cachedImageName = nil;
    
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    
    if ([key hasPrefix:@"/"]) {
        cachedImageName = [NSString stringWithFormat:@"%@%@", self.cacheDirectoryPath, key];
    } else {
        cachedImageName = [NSString stringWithFormat:@"%@/%@", self.cacheDirectoryPath, key];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachedImageName]) {
        RRLog(@"Image is absent by path %@", cachedImageName);
    }
    
    UIImage* cachedImage = [UIImage imageWithContentsOfFile:cachedImageName];
    return cachedImage;
}

#pragma mark - Private

- (void)initializeCachedDirectory {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachesDirectory = paths.firstObject;
    
    NSString* cacheDirectoryName = @"ImagesCache";
    _cacheDirectoryPath = [NSString stringWithFormat:@"%@/%@", cachesDirectory, cacheDirectoryName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:_cacheDirectoryPath]) {
        NSError* error;
        [fileManager createDirectoryAtPath:_cacheDirectoryPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:&error];
        if (error) {
            RRLog(@"Can't create directory. Error %@", error.localizedDescription);
        }
    }
}

- (void)initializeCachedImages {
    
}

#pragma mark - NetworkManagerDelegate

- (void)networkRequestPerformedSuccesfullyWithDownloadedImage:(UIImage*)image {
    
    [self cacheImage:image byKey:self.currentImageKey];
    
    if ([self.delegate respondsToSelector:@selector(imageManagerDownloadedImage:)]) {
        [self.delegate imageManagerDownloadedImage:image];
    }
}

- (void)networkRequestFailedWithError:(NSError*)error {
    if ([self.delegate respondsToSelector:@selector(imageManagerDidNotDonwloadImageWithError:)]) {
        [self.delegate imageManagerDidNotDonwloadImageWithError:error];
    }
}

@end

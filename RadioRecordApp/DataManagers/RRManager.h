//
//  RRManager.h
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RadioStation.h"
#import "TopSong.h"
#import "HistorySong.h"

@class AVPlayer;

typedef enum {
    AudioTypeMP3 = 0,
    AudioTypeAAC
} AudioType;

typedef enum {
    AudioQualityLow = 0,
    AudioQualityHigh
} AudioQuality;

@interface RRManager : NSObject

@property (nonatomic, readonly, strong) NSArray<RadioStation*>* radioStations;
@property (nonatomic, strong) RadioStation* selectedRadioStation;
@property (nonatomic, strong) AVPlayer* player;

+ (id)sharedInstance;
- (void)fetchListOfStationsWithCompletionBlock:(void(^)(NSArray<RadioStation*>* radioStatios))completeBlock
                                 andErrorBlock:(void(^)(NSError* error))errorBlock;

- (void)fetchTop100SongsForRadioStation:(RadioStation*)radioStation withCompletionBlock:(void(^)(NSArray<TopSong*>* topSongs))completionBlock;
- (void)fetchTop100SongsForSelectedRadioStationWithCompletionBlock:(void(^)(NSArray<TopSong*>* topSongs))completionBlock;

- (void)fetchSongsByDate:(NSDate*)date
         forRadioStation:(RadioStation*)radioStation
     withCompletionBlock:(void(^)(NSArray<HistorySong*>* historySongs))completionBlock;
- (void)fetchSongsByDateForSelectedRadioStation:(NSDate*)date
                            withCompletionBlock:(void(^)(NSArray<HistorySong*>* topSongs))completionBlock;

@end

//
//  LastFMManager.m
//  RadioRecordApp
//
//  Created by Admin on 26.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "LastFMManager.h"
#import "NetworkManager.h"

#import "Song.h"
#import "LastFMTrackItem.h"

#import "NSString+Translation.h"

static NSString* const kLastFMAPIKey = @"00efa39f966fc042599c877832494de3";

static NSString* const kLastFMRootURL = @"http://ws.audioscrobbler.com/2.0/?";

static NSString* const kLastFMGetTrackInfoEndPoint = @"method=track.getInfo";

@interface LastFMManager () <NetworkManagerDelegate>

@property (strong, nonatomic) NetworkManager* networkManager;
@property (weak, nonatomic) id<LastFMManagerDelegate>delegate;

@end

@implementation LastFMManager

- (void)searchMusicInfoBySongName:(NSString*)songName
                    andArtistName:(NSString*)artistName
                         delegate:(id<LastFMManagerDelegate>)delegate {
    self.delegate = delegate;
    
    if (!songName || !artistName) {
        if ([self.delegate respondsToSelector:@selector(lastFMManagerTrackNotFoundWithError:)]) {
            NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                                 code:10001
                                             userInfo:@{@"error_code":@"10001", @"error_description":@"Nothing to search"}];
            
            [self.delegate lastFMManagerTrackNotFoundWithError:error];
        }
        
        return;
    }
    
    NSString* artistNameLatin = [[artistName toLatin] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString* songNameLatin = [[songName toLatin] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString* searchURLString = [NSString stringWithFormat:@"%@method=track.getInfo&api_key=%@&artist=%@&track=%@&format=json", kLastFMRootURL, kLastFMAPIKey, artistNameLatin, songNameLatin];
    
    if (!self.networkManager) {
        self.networkManager = [NetworkManager new];
    }
    
    [self.networkManager createGetRequestWithURL:[NSURL URLWithString:searchURLString] delegate:self];
}

- (void)searchMusicInfoBySong:(Song*)song withDelegate:(id<LastFMManagerDelegate>)delegate {
    [self searchMusicInfoBySongName:song.title andArtistName:song.artistName delegate:delegate];
}

#pragma mark - NetworkManagerDelegate

- (void)networkRequestPerformedSuccesfullyWithResponse:(NSDictionary *)response {
    //TODO: Handle response
    if (!response) {
        if ([self.delegate respondsToSelector:@selector(lastFMManagerTrackNotFoundWithError:)]) {
            NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                                 code:10002
                                             userInfo:@{@"error_code":@"10002", @"error_description":@"Response is empty"}];
            
            [self.delegate lastFMManagerTrackNotFoundWithError:error];
        }
    }
    
    LastFMTrackItem* lastFMTrackItem = [LastFMTrackItem createItemFromJSON:response[@"track"]];
    if (!lastFMTrackItem && [self.delegate respondsToSelector:@selector(lastFMManagerTrackNotFoundWithError:)]) {
        NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                             code:10000
                                         userInfo:@{@"error_code":@"10000", @"error_description":@"Response from last.fm doesn't contain a song"}];
        
        [self.delegate lastFMManagerTrackNotFoundWithError:error];
    } else {
        if ([self.delegate respondsToSelector:@selector(lastFMManagerTrackItemFound:)]) {
            [self.delegate lastFMManagerTrackItemFound:lastFMTrackItem];
        }
    }
    
}

- (void)networkRequestFailedWithError:(NSError *)error {
    //TODO: Handle error
    
    if ([self.delegate respondsToSelector:@selector(lastFMManagerTrackNotFoundWithError:)]) {
        [self.delegate lastFMManagerTrackNotFoundWithError:error];
    }
}

@end

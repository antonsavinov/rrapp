//
//  iTunesSearchManager.h
//  RadioRecordApp
//
//  Created by Admin on 25.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Song;
@class iTunesSearchItem;

@protocol iTunesSearchManagerDelegate;

@interface iTunesSearchManager : NSObject

- (void)searchMusicInfoBySongName:(NSString*)songName
                    andArtistName:(NSString*)artistName
                         delegate:(id<iTunesSearchManagerDelegate>)delegate;

- (void)searchMusicInfoBySong:(Song*)song withDelegate:(id<iTunesSearchManagerDelegate>)delegate;

@end

@protocol iTunesSearchManagerDelegate <NSObject>

- (void)iTunesSearchManagerSongItemFound:(iTunesSearchItem*)foundedSongItem;
- (void)iTunesSearchManagerSongItemNotFoundWithError:(NSError*)error;

@end

//
//  CoreDataStore.m
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "CoreDataStore.h"
#import "AppDelegate.h"

@implementation CoreDataStore

+ (NSPersistentStoreCoordinator*)coordinator {
    AppDelegate *appDelegate = ((AppDelegate*)[UIApplication sharedApplication].delegate);
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    return appDelegate.container.persistentStoreCoordinator;
#else
    return appDelegate.persistentStoreCoordinator;
#endif
}

+ (NSManagedObjectModel*)managedObjectModel {
    AppDelegate *appDelegate = ((AppDelegate*)[UIApplication sharedApplication].delegate);
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    return appDelegate.container.managedObjectModel;
#else
    return appDelegate.managedObjectModel;
#endif
}

+ (NSManagedObjectContext*)context {
    AppDelegate *appDelegate = ((AppDelegate*)[UIApplication sharedApplication].delegate);
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    return appDelegate.container.viewContext;
#else
    return appDelegate.managedObjectContext;
#endif
}

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
+ (NSPersistentContainer*)container {
    return ((AppDelegate*)[UIApplication sharedApplication].delegate).container;
}
#endif

@end

//
//  SongLocalStorage.m
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import "SongLocalStorage.h"
#import "CoreDataStore.h"

#import "SongModel+CoreDataProperties.h"
#import "Song.h"

#import "NSString+Translation.h"

@implementation SongLocalStorage

+ (void)saveSong:(Song*)song {
    NSManagedObjectContext* context = [CoreDataStore context];
    if (!context) {
        return;
    }
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"SongModel" inManagedObjectContext:context];
    if (!entity) {
        return;
    }
    
    SongModel* songModel = [[SongModel alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    songModel.artistName = song.artistName;
    songModel.title = song.title;
    songModel.primaryGenreName = song.primaryGenreName;
    songModel.releaseDate = song.releaseDate;
    songModel.trackViewURL = song.trackViewURL;
    songModel.trackId = song.trackId;
    songModel.albumSmallImageLink = song.albumSmallImageLink;
    songModel.albumMediumImageLink = song.albumMediumImageLink;
    songModel.albumLargeImageLink = song.albumExtraLargeImageLink;
    songModel.albumExtraLargeImageLink = song.albumExtraLargeImageLink;
    songModel.albumArtwork100 = song.albumArtwork100;
    songModel.albumArtwork300 = song.albumArtwork300;
    songModel.albumArtwork600 = song.albumArtwork600;
    
    NSError* saveError = nil;
    if(![context save:&saveError]) {
        RRLog(@"Can't save song to storage. Error: %@", saveError.localizedDescription);
    }
}

+ (Song*)retriveSongByTitle:(NSString*)title andArtistName:(NSString*)artistName {
    NSManagedObjectContext* context = [CoreDataStore context];
    if (!context) {
        return nil;
    }
    
    NSFetchRequest *request = [NSFetchRequest new];
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"title == %@ AND artistName == %@", [title toLatin], [artistName toLatin]];
    request.predicate = filter;
    request.entity = [NSEntityDescription entityForName:@"SongModel" inManagedObjectContext:context];
    
    NSError* fetchError = nil;
    NSArray<SongModel*>* songModels = [context executeFetchRequest:request error:&fetchError];
    if (fetchError) {
        RRLog(@"Can't fetch. Error: %@", fetchError.localizedDescription);
        return nil;
    }
    
    SongModel* songModel = songModels.firstObject;
    if (!songModel) {
        RRLog(@"No songs by this request!");
        return nil;
    }
    
    RRLog(@"Song %@-%@ found in storage", songModel.title, songModel.artistName);

    Song* song = [Song songFromSongModel:songModel];
    return song;
}

@end

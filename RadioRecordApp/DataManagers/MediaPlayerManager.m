//
//  PlayerManager.m
//  RadioRecordApp
//
//  Created by Admin on 12.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaItem.h>

#import <UIKit/UIImage.h>
#import <UIKit/UIApplication.h>

#import "MediaPlayerManager.h"
#import "InternetConnectionManager.h"

#import "NSString+Utils.h"

#import "Song.h"

static NSString* const kTimedMetaDataNotification          = @"timedMetadata";
static NSString* const kPlaybackBufferEmptyNotification    = @"playbackBufferEmpty";
static NSString* const kPlaybackLikelyToKeepUpNotification = @"playbackLikelyToKeepUp";

//TODO: A.S.: https://www.klundberg.com/blog/notifying-many-delegates-at-once-with-multicast-delegates/
//TODO: A.S.: https://arielelkin.github.io/articles/objective-c-multicast-delegate.html

@interface MediaPlayerManager ()

@property (strong, nonatomic) AVQueuePlayer* player;
@property (strong, nonatomic) AVPlayerItem* radioStreamItem;
@property (strong, nonatomic) AVPlayerItem* quietSoundItem;
@property (strong, nonatomic) NSArray<AVPlayerItem*>* items;

@property (strong, nonatomic) NSURL* quietSoundItemURL;

@property (weak, nonatomic) id <MediaPlayerManagerDelegate> delegate;

@property (assign, nonatomic) UIBackgroundTaskIdentifier task;

@end

@implementation MediaPlayerManager

#pragma mark - Life Cycle

- (instancetype)init {
    
    if (self = [super init]) {
//#if TARGET_OS_SIMULATOR
        NSString *path = [[NSBundle mainBundle] pathForResource:@"quiet_sound" ofType:@"wav"];
//#else
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"quiet_sound" ofType:@"wav" inDirectory:@"Sounds"];
//#endif
        _quietSoundItemURL = [NSURL fileURLWithPath:path];
        
        [self configureAudioSession];
        [self subcribeNotification];
    }
    return self;
}

+ (instancetype)sharedInstance {
    
    static MediaPlayerManager* mediaPlayerManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        mediaPlayerManager = [[self alloc] init];
    });
    
    return mediaPlayerManager;
}

#pragma mark - Public

- (void)setDelegate:(id<MediaPlayerManagerDelegate>)delegate {
    _delegate = delegate;
}

- (void)playMusicByURL:(NSString*)urlString {
    
    NSString *convertedString = [urlString convertSpacesIfNeeded];
    NSURL *url = [NSURL URLWithString:convertedString];
    
    NSError* error = [[InternetConnectionManager sharedInstance] isInternetConnectionBroken];
    if (error) {
        [self.delegate mediaPlayerCanNotPlayRadioWithError:error];
        return;
    }
    
    self.radioStreamItem = [AVPlayerItem playerItemWithURL:url];
//    self.quietSoundItem = [AVPlayerItem playerItemWithURL:self.quietSoundItemURL];
    
    self.items = @[self.radioStreamItem/*, self.quietSoundItem*/];
    for (AVPlayerItem* playerItem in self.items) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:playerItem];
    }
    
    self.player = [AVQueuePlayer queuePlayerWithItems:self.items];
    
    [self.player play];
    
    [[MediaPlayerManager sharedInstance] startObservingStreamInfo];
}

- (void)stopPlaying {
    
    [self stopObservingStreamInfo];
    
    [self.player pause];
    
    for (AVPlayerItem* playerItem in self.items) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:playerItem];
    }
    
    [self cleanVariables];
}

- (BOOL)isPlayingNow {
    
    return self.player.rate != 0 && !self.player.error ? YES : NO;
}

- (BOOL)isAlreadyPlayingByURL:(NSString*)urlString {

    NSString *convertedString = [urlString convertSpacesIfNeeded];
    NSURL *url = [NSURL URLWithString:convertedString];

    return [url isEqual:[self urlOfCurrentlyPlaying]] ? YES : NO;
}

- (void)startObservingStreamInfo {
    
    RRLog(@"startObservingStreamInfo for url: %@", [self urlOfCurrentlyPlaying]);
    
    for (AVPlayerItem* item in self.items) {
        [item addObserver:self
               forKeyPath:kTimedMetaDataNotification
                  options:NSKeyValueObservingOptionInitial
                  context:nil];
        [item addObserver:self
               forKeyPath:kPlaybackBufferEmptyNotification
                  options:NSKeyValueObservingOptionInitial
                  context:nil];
        [item addObserver:self
               forKeyPath:kPlaybackLikelyToKeepUpNotification
                  options:NSKeyValueObservingOptionInitial
                  context:nil];
    }
}

- (void)stopObservingStreamInfo {
    
    RRLog(@"stopObservingStreamInfo for url: %@", [self urlOfCurrentlyPlaying]);
    
    for (AVPlayerItem* item in self.items) {
        [item removeObserver:self forKeyPath:kTimedMetaDataNotification];
        [item removeObserver:self forKeyPath:kPlaybackBufferEmptyNotification];
        [item removeObserver:self forKeyPath:kPlaybackLikelyToKeepUpNotification];
    }
}

- (void)restartObservingStreamInfo {
    [self stopObservingStreamInfo];
    [self startObservingStreamInfo];
}

- (CGFloat)volume {
    return [AVAudioSession sharedInstance].outputVolume;
}

#pragma mark - Private

- (void) configureAudioSession {
    NSError* sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
}

- (void) subcribeNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAudioSessionInterruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:[AVAudioSession sharedInstance]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMediaServicesReset)
                                                 name:AVAudioSessionMediaServicesWereResetNotification
                                               object:[AVAudioSession sharedInstance]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioHardwareRouteChanged:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:[AVAudioSession sharedInstance]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetConnectionStatusUpdated:)
                                                 name:kInternetConnectionStatus
                                               object:nil];
}

- (NSURL *)urlOfCurrentlyPlaying {
    
    AVAsset *currentPlayerAsset = self.player.currentItem.asset;
    
    return [currentPlayerAsset isKindOfClass:AVURLAsset.class] ? [(AVURLAsset *)currentPlayerAsset URL] : nil;
    
}

- (void)cleanVariables {
    self.items = nil;
    self.quietSoundItem = nil;
    self.radioStreamItem = nil;
    self.player = nil;
    self.delegate = nil;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (object != self.radioStreamItem) {
            return;
        }
        
        if ([keyPath isEqualToString:kTimedMetaDataNotification]) {
            
            NSArray<AVMetadataItem*>* timedMetadata = self.radioStreamItem.timedMetadata;
            
            AVMetadataItem* lastMetaData = [timedMetadata lastObject];
            Song* currentSong = [Song songFromMetaData:lastMetaData];
            
            NSMutableDictionary* lockScreenInfo = [NSMutableDictionary new];
            
            UIImage *albumArtImage = [UIImage imageNamed:@"lock_screen_bg"];
            MPMediaItemArtwork* albumArt = [[MPMediaItemArtwork alloc] initWithImage:albumArtImage];
            if (albumArt) {
                lockScreenInfo[MPMediaItemPropertyArtwork] = albumArt;
            }
            
            if (currentSong.isValid) {
                lockScreenInfo[MPMediaItemPropertyTitle] = currentSong.title;
                lockScreenInfo[MPMediaItemPropertyArtist] = currentSong.artistName;
            }
            
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = lockScreenInfo;
            
            Float64 duration = CMTimeGetSeconds(self.radioStreamItem.duration);
            [currentSong proceedDuration:duration];
            
            if ([self.delegate respondsToSelector:@selector(streamInfoLoadedWithSong:)]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate streamInfoLoadedWithSong:currentSong];
                });
            }
            
            RRLog(@"Song duration:");
            CMTimeShow(self.radioStreamItem.duration);
            
        } else if ([keyPath isEqualToString:kPlaybackBufferEmptyNotification]) {
            
            if (self.radioStreamItem.playbackBufferEmpty) {
                RRLog(@"Buffer Empty");
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                        __block UIBackgroundTaskIdentifier backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(void) {
                            
                        }];
                        
                        self.task = backgroundTask;
                        
                        [self.player advanceToNextItem];

                    } else {
                        //TODO: A.S.: Show something on UI
                    }
                });
                
            } else {
                RRLog(@"Buffer not Empty");
            }
            
        } else if ([keyPath isEqualToString:kPlaybackLikelyToKeepUpNotification]) {

            if (self.radioStreamItem.playbackLikelyToKeepUp) {

                RRLog(@"LikelyToKeepUp");
                dispatch_async(dispatch_get_main_queue(), ^{

                    if ([self.delegate respondsToSelector:@selector(mediaPlayerStreamBecameAvailable)]) {
                        [self.delegate mediaPlayerStreamBecameAvailable];
                    }


                    [self.player play];

                    if (self.task != UIBackgroundTaskInvalid) {
                        [[UIApplication sharedApplication] endBackgroundTask:self.task];
                        self.task = UIBackgroundTaskInvalid;
                    }
                });
                
            } else {
                RRLog(@"UnLikelyToKeepUp");
                
                NSError* error = [[InternetConnectionManager sharedInstance] isInternetConnectionBroken];
                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate mediaPlayerCanNotPlayRadioWithError:error];
                    });
                }
            }
        }
    });
}

#pragma mark - Selectors

- (void) internetConnectionStatusUpdated:(NSNotification*)notification {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        BOOL isInternetAvailable = [notification.userInfo[@"Status"] boolValue];
        if (isInternetAvailable && self.radioStreamItem) {
            
            if (self.task != UIBackgroundTaskInvalid) {
                
                AVPlayerItem *playerItem = [self.player currentItem];
                
                // Set it back to the beginning
                [playerItem seekToTime: kCMTimeZero];
                // Tell the player to do nothing when it reaches the end of the video
                //  -- It will come back to this method when it's done
                self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                // Play it again, Sam
                [self.player play];
                
                [[UIApplication sharedApplication] endBackgroundTask:self.task];
                self.task = UIBackgroundTaskInvalid;
                
            }
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification*)notification {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        BOOL isInternetAvailable = [notification.userInfo[@"Status"] boolValue];
        if (isInternetAvailable && self.radioStreamItem) {
            
            if (self.task != UIBackgroundTaskInvalid) {
                
                AVPlayerItem *playerItem = [self.player currentItem];
                
                // Set it back to the beginning
                [playerItem seekToTime: kCMTimeZero];
                // Tell the player to do nothing when it reaches the end of the video
                //  -- It will come back to this method when it's done
                self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                // Play it again, Sam
                [self.player play];
                
                [[UIApplication sharedApplication] endBackgroundTask:self.task];
                self.task = UIBackgroundTaskInvalid;
            }
        }
    } else if ([notification.name isEqualToString:@"AVPlayerItemDidPlayToEndTimeNotification"]) {
        if ([self.delegate respondsToSelector:@selector(mediaPlayerFinishedPlaying)]) {
            [self.delegate mediaPlayerFinishedPlaying];
        }
        [self stopPlaying];
    }
}

- (void)handleAudioSessionInterruption:(NSNotification*)notification {
    NSDictionary* userInfo = notification.userInfo;
    
    NSUInteger interaptionType = [userInfo[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    NSUInteger interruptionOption = [userInfo[AVAudioSessionInterruptionOptionKey] unsignedIntegerValue];
    
    switch (interaptionType) {
        case AVAudioSessionInterruptionTypeBegan:
            //TODO: A.S. Show something on UI
            break;
        case AVAudioSessionInterruptionTypeEnded: {
            if (interruptionOption == AVAudioSessionInterruptionOptionShouldResume) {
                //TODO: A.S. Show something on UI
                
                //TODO: A.S. Restore AVAudioSession
                //[self configureAudioSession];
                
                [self.player play];
            }
        } break;
        default:
            break;
    }
}

- (void)handleMediaServicesReset {
    //TODO: A.S. Restore AVAudioSession
    [self configureAudioSession];
    [self.player play];
}

- (void)audioHardwareRouteChanged:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    NSInteger routeChangeReason = [userInfo[AVAudioSessionRouteChangeReasonKey] unsignedIntegerValue];
    if (routeChangeReason == AVAudioSessionRouteChangeReasonOldDeviceUnavailable) {
        [self.player play];
    }
}

@end

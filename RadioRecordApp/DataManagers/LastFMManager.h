//
//  LastFMManager.h
//  RadioRecordApp
//
//  Created by Admin on 26.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Song;
@class LastFMTrackItem;
@protocol LastFMManagerDelegate;

@interface LastFMManager : NSObject

- (void)searchMusicInfoBySongName:(NSString*)songName
                    andArtistName:(NSString*)artistName
                         delegate:(id<LastFMManagerDelegate>)delegate;

- (void)searchMusicInfoBySong:(Song*)song withDelegate:(id<LastFMManagerDelegate>)delegate;

@end

@protocol LastFMManagerDelegate <NSObject>

- (void)lastFMManagerTrackItemFound:(LastFMTrackItem*)foundedSongItem;
- (void)lastFMManagerTrackNotFoundWithError:(NSError*)error;

@end

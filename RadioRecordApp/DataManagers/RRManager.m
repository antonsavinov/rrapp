//
//  RRManager.m
//  RadioRecordApp
//
//  Created by Admin on 20.11.16.
//  Copyright © 2016 Savinov.Anton. All rights reserved.
//

#import "RRManager.h"

#import "TFHpple.h"

#import "TopSong.h"
#import "HistorySong.h"

@interface RRManager ()

@end

@implementation RRManager

- (instancetype)init {
    
    if (self = [super init]) {
    }
    return self;
}

+ (instancetype)sharedInstance {
    
    static RRManager* rrManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        rrManager = [[self alloc] init];
    });
    
    return rrManager;
}

- (void)fetchListOfStationsWithCompletionBlock:(void(^)(NSArray<RadioStation*>* radioStatios))completeBlock
                                 andErrorBlock:(void(^)(NSError* error))errorBlock {
    
    //TODO: A.S.: Imitate server request
    dispatch_queue_t queue = dispatch_queue_create("radiorecordapp.fetchstationsqueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(queue, ^{
        NSString * filePath = [[NSBundle mainBundle] pathForResource:@"RadioStationsInfo"
                                                              ofType:@"json"];
        
        sleep(5);
        
        NSError* error;
        NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                           encoding:NSUTF8StringEncoding
                                                              error:&error];
        
        if(error) {
            RRLog(@"Error contents by file path: %@", error.localizedDescription);
            if (errorBlock) {
                errorBlock(error);
            }
            return;
        }
        
        NSArray<NSDictionary*>* radioStationJSONObjects =
        [NSJSONSerialization JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                        options:NSJSONReadingAllowFragments
                                          error:&error];
        
        if (error) {
            RRLog(@"Error parse file: %@", error.localizedDescription);
            if (errorBlock) {
                errorBlock(error);
            }
            return;
        }
        
        if (completeBlock) {
            
            NSArray<RadioStation*>* radioStations = [RadioStation radioStantionsFromJSONObjects:radioStationJSONObjects];
            completeBlock(radioStations);
        }
    });
}

- (void)fetchTop100SongsForRadioStation:(RadioStation*)radioStation
                    withCompletionBlock:(void(^)(NSArray<TopSong*>* topSongs))completionBlock {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSString* remotedTop100SongsURLString = [NSString stringWithFormat:@"http://www.radiorecord.ru/radio/top100/%@.txt", radioStation.identifier];
        
        NSURL *remotedTop100SongsURL = [NSURL URLWithString:remotedTop100SongsURLString];
        NSData *remotedTop100SongsURLHtmlData = [NSData dataWithContentsOfURL:remotedTop100SongsURL];

        TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:remotedTop100SongsURLHtmlData];
        
        NSString *tutorialsXpathQueryString = @"//article[@class='track-holder top100']/div[@class='player-raiting-holder']/div [@class='player_wrapper']/table[@class='player']/tbody/tr";
        NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
        
        NSMutableArray<TopSong*>* top100Songs = [NSMutableArray new];
        
        for (TFHppleElement* tr in tutorialsNodes) {
            
            NSArray<TFHppleElement*>* trChildren = tr.children;

#if TARGET_IPHONE_SIMULATOR
            NSArray<TFHppleElement*>* nodeTd1 = trChildren[1].children;
#else
            NSArray<TFHppleElement*>* nodeTd1 = trChildren[0].children;
#endif
            NSString* url = nil;
            if (nodeTd1.count > 1) {
                TFHppleElement* nodeA = nodeTd1[1];
                url = [nodeA objectForKey:@"href"];
            }
#if TARGET_IPHONE_SIMULATOR
            NSArray<TFHppleElement*>* nodeTd2 = trChildren[3].children;
#else
            NSArray<TFHppleElement*>* nodeTd2 = trChildren[2].children;
#endif
            NSString* artistName = nil;
            if (nodeTd2.count > 1) {
                TFHppleElement* nodeArtist = nodeTd2[1];
                artistName = nodeArtist.content;
            }
            
            NSString* songName = nil;
            if (nodeTd2.count > 3) {
                TFHppleElement* nodeSongName = nodeTd2[3];
                songName = nodeSongName.content;
            }

            if (!url || !artistName || !songName) {
                continue;
            }

            TopSong* topSong = [TopSong new];
            topSong.artistName = artistName;
            topSong.songTitle = songName;
            topSong.remoteUrlForPlaying = url;
            
            [top100Songs addObject:topSong];
        }
        
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(top100Songs);
            });
        }
        
    });
}

- (void)fetchTop100SongsForSelectedRadioStationWithCompletionBlock:(void(^)(NSArray<TopSong*>* topSongs))completionBlock {
    [self fetchTop100SongsForRadioStation:self.selectedRadioStation withCompletionBlock:completionBlock];
}

- (void)fetchSongsByDate:(NSDate*)date
         forRadioStation:(RadioStation*)radioStation
     withCompletionBlock:(void(^)(NSArray<HistorySong*>* historySongs))completionBlock {

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{

        NSDateFormatter* dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"YYYY-MM-dd";
        NSString *dateString = [dateFormatter stringFromDate:date];

        NSString* songsFromHistoryURLString = [NSString stringWithFormat:@"http://history.radiorecord.ru/index-onstation.php?station=%@&day=%@&time=%@", radioStation.identifier, dateString, @([[NSDate date] timeIntervalSince1970] * 1000)];

        NSURL *songsFromHistoryURL = [NSURL URLWithString:songsFromHistoryURLString];
        NSData *songsFromHistoryURLHtmlData = [NSData dataWithContentsOfURL:songsFromHistoryURL];

        TFHpple *historyParser = [TFHpple hppleWithHTMLData:songsFromHistoryURLHtmlData];

        NSString *songsFromHistoryXpathQueryString = @"//article[@class='track-holder history ']/div[@class='player-raiting-holder']/div[@class='player_wrapper']/table[@class='player']/tbody/tr";
        NSArray *songsFromHistoryNodes = [historyParser searchWithXPathQuery:songsFromHistoryXpathQueryString];

        NSString *songsFromHistoryTimesXpathQueryString = @"//article[@class='track-holder history ']/div[@class='player-raiting-holder']/div[@class='place']";

        NSArray *songsFromHistoryTimesNodes = [historyParser searchWithXPathQuery:songsFromHistoryTimesXpathQueryString];

        NSMutableArray<HistorySong*>* songsFromHistory = [NSMutableArray new];

        for (NSUInteger index = 0; index < songsFromHistoryNodes.count; index++) {

            NSArray<TFHppleElement*>* trChildren = [songsFromHistoryNodes[index] children];

            NSString* url = nil;
#if TARGET_IPHONE_SIMULATOR
            TFHppleElement *nodeTdPlayPause = trChildren[1];
#else
            TFHppleElement* nodeTdPlayPause = trChildren[0];
#endif
            if ([nodeTdPlayPause objectForKey:@"item_url"]) {
                url = [nodeTdPlayPause objectForKey:@"item_url"];
            }

#if TARGET_IPHONE_SIMULATOR
            NSArray<TFHppleElement*>* nodeTdPlayInfo = trChildren[3].children;
#else
            NSArray<TFHppleElement*>* nodeTdPlayInfo = trChildren[2].children;
#endif

            NSString* artistName = nil;
            if (nodeTdPlayInfo.count > 1) {
                TFHppleElement* nodeArtist = nodeTdPlayInfo[1];
                artistName = nodeArtist.content;
            }

            NSString* songName = nil;
            if (nodeTdPlayInfo.count > 3) {
                TFHppleElement* nodeSongName = nodeTdPlayInfo[3];
                songName = nodeSongName.content;
            }

            NSString* startTime = nil;
            if ([songsFromHistoryTimesNodes objectAtIndex:index]) {
                TFHppleElement* nodeTime = songsFromHistoryTimesNodes[index];
                startTime = nodeTime.content;
            }

            if (!url || !artistName || !songName) {
                continue;
            }

            HistorySong* songFromHistory = [HistorySong new];
            songFromHistory.artistName = artistName;
            songFromHistory.songTitle = songName;
            songFromHistory.remoteUrlForPlaying = url;
            songFromHistory.startTime = startTime;

            [songsFromHistory addObject:songFromHistory];
        }

        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(songsFromHistory);
            });
        }
    });
}

- (void)fetchSongsByDateForSelectedRadioStation:(NSDate*)date
                            withCompletionBlock:(void(^)(NSArray<HistorySong*>* topSongs))completionBlock {
    [self fetchSongsByDate:date forRadioStation:self.selectedRadioStation withCompletionBlock:completionBlock];
}

@end

//
//  NetworkManagerDelegate.h
//  RadioRecordApp
//
//  Created by Admin on 25.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@protocol NetworkManagerDelegate <NSObject>

- (void)networkRequestFailedWithError:(NSError*)error;

@optional
- (void)networkRequestPerformedSuccesfullyWithResponse:(NSDictionary*)response;
- (void)networkRequestPerformedSuccesfullyWithDownloadedImage:(UIImage*)image;

@end

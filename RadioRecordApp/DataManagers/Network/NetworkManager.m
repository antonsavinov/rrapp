//
//  NetworkManager.m
//  RadioRecordApp
//
//  Created by Admin on 25.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <UIKit/UIImage.h>

#import "NetworkManager.h"

#import "NSData+Utils.h"

@interface NetworkManager ()

@property (nonatomic, weak) id<NetworkManagerDelegate>delegate;
@property (nonatomic, strong) NSMutableDictionary *dataDictionary;

@end

@implementation NetworkManager

- (instancetype)init {
    if (self = [super init]) {
        _dataDictionary = [NSMutableDictionary new];
    }
    return self;
}

#pragma mark - Public

- (void)createGetRequestWithURL:(NSURL*)url delegate:(id<NetworkManagerDelegate>)delegate {
    
    self.delegate = delegate;
    
    RRLog(@"NetworkManager: url: %@", url);
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self createGetRequestByConnectionWithURL:url];
    } else {
        [self createGetRequestBySessionTaskWithURL:url];
    }
}

- (void)downloadImageWithURL:(NSURL *)url delegate:(id<NetworkManagerDelegate>)delegate {
    
    self.delegate = delegate;
    
    RRLog(@"NetworkManager: url: %@", url);
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self downloadImageByConnectionWithURL:url];
    } else {
        [self downloadImageBySessionTaskWithURL:url];
    }
}

#pragma mark - Private

- (void)createGetRequestByConnectionWithURL:(NSURL*)url {
    
    NSMutableData *receivedData = [NSMutableData new];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:self startImmediately:NO];
    self.dataDictionary[[connection description]] = receivedData;
    
    [connection start];
    
    RRLog(@"Create get request by connection for url %@", connection.originalRequest);
}

- (void)createGetRequestBySessionTaskWithURL:(NSURL*)url {
    
    NSURLSessionDataTask *downloadTask =
    [[NSURLSession sharedSession] dataTaskWithURL:url
                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (error) {
            [self sendError:error];
        } else {
            [self performSerializationRawDatas:data];
        }
    }];
    
    [downloadTask resume];
    
    RRLog(@"Create get request by session task for url %@", downloadTask.originalRequest);
}

- (void)downloadImageByConnectionWithURL:(NSURL*)url {
    [self createGetRequestByConnectionWithURL:url];
}

- (void)downloadImageBySessionTaskWithURL:(NSURL*)url {
    NSURLSessionDownloadTask* downloadImageTask =
    [[NSURLSession sharedSession] downloadTaskWithURL:url
                                    completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
    {
        if (error) {
            [self sendError:error];
        } else {
            NSData* dataByLocation = [NSData dataWithContentsOfURL:location];
            UIImage* downloadedImage = [UIImage imageWithData:dataByLocation];
            [self sendResponseWithImage:downloadedImage];
        }
    }];
    [downloadImageTask resume];
}

- (void)performSerializationRawDatas:(NSData*)data {
    NSString* mimeType = [data mimeType];
    if ([mimeType isEqualToString:@"application/octet-stream"]) {
        NSError* jsonSerializationError = nil;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonSerializationError];
        if (jsonSerializationError) {
            [self sendError:jsonSerializationError];
        } else {
            [self sendResponse:response];
        }
    } else {
        //A.S.: Now we are calculating others response as Image
        UIImage* image = [UIImage imageWithData:data];
        [self sendResponseWithImage:image];
    }
}

- (void)sendResponse:(NSDictionary*)response {
    
    RRLog(@"NetworkManager: response: %@", response);
    
    if ([self.delegate respondsToSelector:@selector(networkRequestPerformedSuccesfullyWithResponse:)]) {
        [self.delegate networkRequestPerformedSuccesfullyWithResponse:response];
    }
}

- (void)sendResponseWithImage:(UIImage*)image {
    RRLog(@"NetworkManager: image response: %@", NSStringFromCGSize(image.size));
    
    if ([self.delegate respondsToSelector:@selector(networkRequestPerformedSuccesfullyWithDownloadedImage:)]) {
        [self.delegate networkRequestPerformedSuccesfullyWithDownloadedImage:image];
    }
}

- (void)sendError:(NSError*)error {
    
    RRLog(@"NetworkManager: error: %@", [error localizedDescription]);
    
    if ([self.delegate respondsToSelector:@selector(networkRequestFailedWithError:)]) {
        [self.delegate networkRequestFailedWithError:error];
    }
}

#pragma mark - NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    
    NSMutableData *theReceivedData = self.dataDictionary[[connection description]];
    [theReceivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    
    //get the object for the connection that has been passed to connectionDidRecieveData and that object will be the data variable for that instance of the connection.
    NSMutableData *theReceivedData = self.dataDictionary[[connection description]];
    
    //then act on that data
    [theReceivedData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSMutableData *theReceivedData = self.dataDictionary[[connection description]];
    [self performSerializationRawDatas:theReceivedData];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [self sendError:error];
}


@end

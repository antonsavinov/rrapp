//
//  NetworkManager.h
//  RadioRecordApp
//
//  Created by Admin on 25.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkManagerDelegate.h"

@interface NetworkManager : NSObject

- (void)createGetRequestWithURL:(NSURL*)url delegate:(id<NetworkManagerDelegate>)delegate;
- (void)downloadImageWithURL:(NSURL*)url delegate:(id<NetworkManagerDelegate>)delegate;

@end

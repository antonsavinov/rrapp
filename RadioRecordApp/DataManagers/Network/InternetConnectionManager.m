//
//  InternetConnectionManager.m
//  RadioRecordApp
//
//  Created by Admin on 16.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "InternetConnectionManager.h"
#import "Reachability.h"

NSString* const kInternetConnectionStatus = @"kInternetConnectionStatus";

@interface InternetConnectionManager ()

@property (strong, nonatomic) Reachability* internetConnectionReachability;

@end

@implementation InternetConnectionManager

#pragma mark - Lyfe Cycle

- (instancetype)init {
    
    if (self = [super init]) {
        _internetConnectionReachability = [Reachability reachabilityForInternetConnection];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        [_internetConnectionReachability startNotifier];
    }
    return self;
}

+ (instancetype)sharedInstance {
    
    static InternetConnectionManager* internetConnectionManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        internetConnectionManager = [[self alloc] init];
    });
    
    return internetConnectionManager;
}

#pragma mark - Public

- (NSError*)isInternetConnectionBroken {
    if ([self.internetConnectionReachability currentReachabilityStatus] == NotReachable) {
        NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                             code:10100
                                         userInfo:@{@"error_code":@"10100",
                                                    @"error_description" : Localized(@"IDS_INTERNET_CONNECTION_MANAGER_INTERNET_IS_BROKEN")}];
        return error;
    }
    
    return nil;
}

#pragma mark - Selectors

- (void)reachabilityChanged {
    if ([self.internetConnectionReachability currentReachabilityStatus] == NotReachable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kInternetConnectionStatus
                                                            object:nil
                                                          userInfo:@{@"Status":@(NO)}];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kInternetConnectionStatus
                                                            object:nil
                                                          userInfo:@{@"Status":@(YES)}];
    }
}

@end

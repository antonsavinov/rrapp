//
//  InternetConnectionManager.h
//  RadioRecordApp
//
//  Created by Admin on 16.10.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kInternetConnectionStatus;

@interface InternetConnectionManager : NSObject

+ (instancetype)sharedInstance;
- (NSError*)isInternetConnectionBroken;

@end

//
//  CoreDataStore.h
//  RadioRecordApp
//
//  Created by Admin on 03.01.18.
//  Copyright © 2018 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataStore : NSObject

+ (NSPersistentStoreCoordinator*)coordinator;
+ (NSManagedObjectModel*)managedObjectModel;
+ (NSManagedObjectContext*)context;
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
+ (NSPersistentContainer*)container;
#endif

@end

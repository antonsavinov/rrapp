//
//  ImageManager.h
//  RadioRecordApp
//
//  Created by Admin on 12.07.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@protocol ImageManagerDelegate;

@interface ImageManager : NSObject

- (void)downloadImageByURL:(NSURL*)url delegate:(id<ImageManagerDelegate>)delegate;
- (void)cacheImage:(UIImage*)image byKey:(NSString*)key;
- (UIImage*)cachedImageByKey:(NSString*)key;

@end

@protocol ImageManagerDelegate <NSObject>

- (void)imageManagerDownloadedImage:(UIImage*)image;
- (void)imageManagerDidNotDonwloadImageWithError:(NSError*)error;

@end
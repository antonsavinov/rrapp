//
//  iTunesSearchManager.m
//  RadioRecordApp
//
//  Created by Admin on 25.06.17.
//  Copyright © 2017 Savinov.Anton. All rights reserved.
//

#import "NetworkManager.h"
#import "iTunesSearchManager.h"

#import "iTunesSearchItem.h"

#import "Song.h"

#import "NSString+Translation.h"
#import "SongLocalStorage.h"

static NSString* const iTunesSearchAPIEntryPoint = @"https://itunes.apple.com/search";

@interface iTunesSearchManager () <NetworkManagerDelegate>

@property (strong, nonatomic) NetworkManager* networkManager;
@property (weak, nonatomic) id<iTunesSearchManagerDelegate>delegate;

@end

@implementation iTunesSearchManager

- (void)searchMusicInfoBySong:(Song*)song withDelegate:(id<iTunesSearchManagerDelegate>)delegate {
    [self searchMusicInfoBySongName:song.title andArtistName:song.artistName delegate:delegate];
}

- (void)searchMusicInfoBySongName:(NSString *)songName andArtistName:(NSString *)artistName delegate:(id<iTunesSearchManagerDelegate>)delegate {
    
    self.delegate = delegate;
    
    if (!songName || !artistName) {
        if ([self.delegate respondsToSelector:@selector(iTunesSearchManagerSongItemNotFoundWithError:)]) {
            NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                                 code:10001
                                             userInfo:@{@"error_code":@"10001", @"error_description":@"Nothing to search"}];
            
            [self.delegate iTunesSearchManagerSongItemNotFoundWithError:error];
        }
        
        return;
    }
    
    NSString* artistNameLatin = [artistName toLatin];
    NSString* songNameLatin = [songName toLatin];
        
    NSString* term =
    [[NSString stringWithFormat:@"%@ %@",artistNameLatin, songNameLatin] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString* searchURLString = [NSString stringWithFormat:@"%@?term=%@&country=ru",iTunesSearchAPIEntryPoint, term];
    
    if (!self.networkManager) {
        self.networkManager = [NetworkManager new];
    }
    
    [self.networkManager createGetRequestWithURL:[NSURL URLWithString:searchURLString] delegate:self];
}

#pragma mark - NetworkManagerDelegate

- (void)networkRequestPerformedSuccesfullyWithResponse:(NSDictionary *)response {
    //TODO: Handle response
    
    NSArray<iTunesSearchItem*>* searchItems = [iTunesSearchItem createItemsFromJSONs:response[@"results"]];
    if (searchItems.count == 0) {
        if ([self.delegate respondsToSelector:@selector(iTunesSearchManagerSongItemNotFoundWithError:)]) {
            NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                                 code:10002
                                             userInfo:@{@"error_code":@"10002", @"error_description":@"Response is empty"}];
            
            [self.delegate iTunesSearchManagerSongItemNotFoundWithError:error];
        }
    }
    iTunesSearchItem* songItem = [iTunesSearchItem firstSongfromCollection:searchItems];
    if (!songItem && [self.delegate respondsToSelector:@selector(iTunesSearchManagerSongItemNotFoundWithError:)]) {
        NSError* error = [NSError errorWithDomain:@"com.radiorecordapp"
                                             code:10000
                                         userInfo:@{@"error_code":@"10000", @"error_description":@"Response from iTunes doesn't contain a song"}];
        
        [self.delegate iTunesSearchManagerSongItemNotFoundWithError:error];
    } else {
        if ([self.delegate respondsToSelector:@selector(iTunesSearchManagerSongItemFound:)]) {
            [self.delegate iTunesSearchManagerSongItemFound:songItem];
        }
    }
}

- (void)networkRequestFailedWithError:(NSError *)error {
    //TODO: Handle error
    
    if ([self.delegate respondsToSelector:@selector(iTunesSearchManagerSongItemNotFoundWithError:)]) {
        [self.delegate iTunesSearchManagerSongItemNotFoundWithError:error];
    }
}

@end
